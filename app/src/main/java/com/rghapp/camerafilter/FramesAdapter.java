package com.rghapp.camerafilter; /**
 * Created by Rasool Ghana on 11/23/17.
 * Email : Rasool.ghana@gmail.com
 */

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

/**
 * Created by Jeffrey Liu on 3/21/16.
 */
public class FramesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    enum FrameTypes{
        Black,
        Colourful,
        Love,
        Other
    }

    private Context mContext;


    private FrameItemsListener mListener;

    private boolean isLocked = true;

    private SparseIntArray mCategoriesImage;

    private FrameTypes mFrameType;

    public FramesAdapter(Context context, boolean isLocked,FrameTypes frameTypes) {
        this.mContext = context;
        this.isLocked = isLocked;
        mCategoriesImage = new SparseIntArray();
        mFrameType = frameTypes;
        loadData();
    }

    private void loadData(){
        if (mFrameType == FrameTypes.Black){
            mCategoriesImage.put(0,R.drawable.frame_black12);
            mCategoriesImage.put(1,R.drawable.frame_black11);
            mCategoriesImage.put(2,R.drawable.frame_black10);
            mCategoriesImage.put(3,R.drawable.frame_black_ink1);
            mCategoriesImage.put(4,R.drawable.frame_black_ink2);
            mCategoriesImage.put(5,R.drawable.frame_black_ink3);
            mCategoriesImage.put(6,R.drawable.frame_black_ink4);
            mCategoriesImage.put(7,R.drawable.frame_black_ink5);
        }
        else if (mFrameType == FrameTypes.Colourful){
            mCategoriesImage.put(0,R.drawable.frame_color1);
            mCategoriesImage.put(1,R.drawable.frame_color2);
            mCategoriesImage.put(2,R.drawable.frame_color3);
            mCategoriesImage.put(3,R.drawable.frame_color4);
            mCategoriesImage.put(4,R.drawable.frame_color5);
            mCategoriesImage.put(5,R.drawable.frame_color6);
            mCategoriesImage.put(6,R.drawable.frame_color7);
            mCategoriesImage.put(7,R.drawable.frame_color8);
            mCategoriesImage.put(8,R.drawable.frame_color9);
            mCategoriesImage.put(9,R.drawable.frame_color10);
        }
        else if (mFrameType == FrameTypes.Love){
            mCategoriesImage.put(0,R.drawable.frame_love1);
            mCategoriesImage.put(1,R.drawable.frame_love2);
            mCategoriesImage.put(2,R.drawable.frame_love3);
            mCategoriesImage.put(3,R.drawable.frame_love_4);
            mCategoriesImage.put(4,R.drawable.frame_love5);
        }
        else if (mFrameType == FrameTypes.Other){
            mCategoriesImage.put(0,R.drawable.frane_special1);
            mCategoriesImage.put(1,R.drawable.frame_special2);
            mCategoriesImage.put(2,R.drawable.frame_special3);
            mCategoriesImage.put(3,R.drawable.frame_special4);
        }
    }

    public int getFrameResourceId(int position){
        return mCategoriesImage.get(position,-1);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null){
            CellViewHolder viewHolder = (CellViewHolder) view.getTag();
            mListener.onFrameItemSelected(viewHolder.getAdapterPosition());
        }
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {

        private AppCompatImageView mFilterImageView;
        private View mFilterContainer;
        private View mLockView;

        public CellViewHolder(View itemView) {
            super(itemView);
            mFilterImageView = itemView.findViewById(R.id.frameCellImageView);
            mFilterContainer = itemView.findViewById(R.id.frameRoot);
            mLockView = itemView.findViewById(R.id.lockView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frames_cell, viewGroup, false);
                CellViewHolder viewHolder = new CellViewHolder(v1);
                viewHolder.mFilterContainer.setOnClickListener(FramesAdapter.this);
                if (isLocked && mFrameType != FrameTypes.Black)
                    viewHolder.mLockView.setVisibility(View.VISIBLE);
                else
                    viewHolder.mLockView.setVisibility(View.GONE);
                return viewHolder;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                final CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.mFilterContainer.setTag(cellViewHolder);
                Picasso.with(mContext)
                        .load(mCategoriesImage.get(position))
                        .noPlaceholder()
                        .fit()
                        .into(cellViewHolder.mFilterImageView);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCategoriesImage.size();
    }

    public interface FrameItemsListener{
        void onFrameItemSelected(int index);
    }

    public void setFrameItemSelectionListener(FrameItemsListener frameItemsListener){
        this.mListener = frameItemsListener;
    }

}