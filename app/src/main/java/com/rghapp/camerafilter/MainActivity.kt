package com.rghapp.camerafilter

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.preference.PreferenceManager
import android.support.constraint.ConstraintSet
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.animation.Animation
import com.appodeal.ads.Appodeal
import com.rghapp.camerafilter.PurchaseHelper.Companion.isApplicationLocked
import com.rghapp.instatext.Utility
import com.squareup.picasso.Picasso
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_aspect_dialog.view.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper




class MainActivity : AppCompatActivity(),FiltersAdapter.FilterSelectionListener,
        AspectRatioAdapter.AspectRatioListener,PreviewFragment.OnSurfaceClickListener,
        FrameCategoryAdapter.FrameCategoryListener,PurchaseHelper.LicenceListener {

    enum class MenuType{
        Filters,
        FrameCategories,
        AspectRatio,
        Unknown
    }

    enum class TimerType{
        OFF,
        Three,
        Ten
    }

    companion object {
         val REQUEST_CAMERA_PERMISSION = 101
         val PHOTO_EDITOR_REQUEST = 1983
         val REQUEST_WRITE_PERMISSION = 102
    }

    private var mCurrentMenuType : MenuType = MenuType.Unknown
    private var mCurrentTimerType : TimerType = TimerType.OFF
    private var mPreviewFragment : PreviewFragment? = null
    private var mCurrentAspectRatio = 0
    private var mIsAddRewardAvailable = false

    lateinit var mSettings : SharedPreferences
    private var mSelectedFilterValues = HashMap<Int,Int>()
    var isCapturingEnabled = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        PurchaseHelper.init(this)
//
        try{
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        } catch (e:Exception){
            e.printStackTrace()
        }

        lastCaptureRefreshView.setStrokeColor(ContextCompat.getColor(this@MainActivity,R.color.loading_stroke_color))
        lastCaptureRefreshView.setGradientStartColor(ContextCompat.getColor(this@MainActivity,R.color.loading_gradient_start))
        lastCaptureRefreshView.setGradientEndColor(ContextCompat.getColor(this@MainActivity,R.color.loading_gradient_end))

        mSettings = PreferenceManager.getDefaultSharedPreferences(this@MainActivity)

        if (!PermissionUtil.shouldAskPermission()) {
            bindPreviewFragment()
        }else{
            PermissionUtil.checkPermission(this,this@MainActivity,
                    Manifest.permission.CAMERA,object: PermissionUtil.PermissionAskListener{
                override fun onNeedPermission() {
                    val PERMISSIONS = arrayOf(android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    ActivityCompat.requestPermissions(this@MainActivity as Activity, PERMISSIONS, REQUEST_CAMERA_PERMISSION )
                }

                override fun onPermissionPreviouslyDenied() {
                    permissionView.visibility = View.VISIBLE
                }

                override fun onPermissionDisabled() {
                    permissionView.visibility = View.VISIBLE
                }

                override fun onPermissionGranted() {
                    bindPreviewFragment()
                }
            })
        }


        if (savedInstanceState != null && mPreviewFragment != null){
            val cameraType = savedInstanceState.getInt("camera_type")
            if (cameraType == 1){
                mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Back
            }
            else if (cameraType == 2) {
                mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Front
            }

            mPreviewFragment!!.mCurrentFilterId = savedInstanceState.getInt("current_filter")
        }

        if (!mSettings.getString(getString(R.string.images_last_path_key),"").isEmpty()){
            Picasso.with(this@MainActivity)
                    .load(mSettings.getString(getString(R.string.images_last_path_key),""))
                    .into(lastCapturedImageView)
            lastCapturedImageView.setOnClickListener {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.type = "image/*"
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }

        captureBtn.setOnClickListener {
            Appodeal.show(this, Appodeal.INTERSTITIAL);
            if (isCapturingEnabled) {
                captureBtn.isEnabled = false
                mIsAddRewardAvailable = false
                if (mCurrentTimerType == TimerType.OFF)
                    handleCapturingProcess()
                else
                    startTimerCountDown()
            }
            else{
                showLicenceDialog()
//                PurchaseHelper.startBuyProcess(this)
            }
        }


        filtersBtn.setOnClickListener {
            toggleSecondMenu(MenuType.Filters)
        }

        aspectRatioBtn.setOnClickListener {
            toggleSecondMenu(MenuType.AspectRatio)
        }

        changeCameraBtn.setOnClickListener {
            if (mPreviewFragment != null) {

                if (mTimerCountDown != null && timerView != null && timerView.visibility == View.VISIBLE){
                    mTimerCountDown?.cancel()
                    timerView.visibility = View.GONE
                }

                mPreviewFragment!!.stopPreview()
                if (mPreviewFragment!!.mCurrentCameraType == CameraRenderer.CameraType.Back)
                    mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Front
                else if (mPreviewFragment!!.mCurrentCameraType == CameraRenderer.CameraType.Front)
                    mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Back
                val ft = supportFragmentManager.beginTransaction()
                ft.detach(mPreviewFragment!!)
                ft.attach(mPreviewFragment!!)
                ft.commit()

                if (mCurrentMenuType == MenuType.AspectRatio)
                    hideSecondaryMenu()

//                if (mPreviewFragment!!.mCurrentCameraType == CameraRenderer.CameraType.Front)
//                    showRotateScreenOption()
            }

        }

        gridBtn.setOnClickListener {
            toggleGridLines()
        }

        timerBtn.setOnClickListener {
            toggleTimer()
        }

        framesBtn.setOnClickListener {
            toggleSecondMenu(MenuType.FrameCategories)
        }
        framesBackBtn.setOnClickListener {
            showSecondaryMenu(MenuType.FrameCategories)
            bottomMenuTitle.text = getString(R.string.bottom_menu_frame_title)
        }

        closeSecondMenuBtn.setOnClickListener {
            hideSecondaryMenu()
        }

        val max = 10
        val min = 0
        val total = max - min

        filterValueSelector.positionListener = { pos ->
            val selectedValue = min + (total  * pos).toInt()
            filterValueSelector.bubbleText = "$selectedValue"
            if (mPreviewFragment != null) {
                mPreviewFragment!!.changeSelectedFilterValue(selectedValue)
                mSelectedFilterValues.put(mPreviewFragment!!.mCurrentFilterId, selectedValue)
            }
        }


        menuPreviewContainer.setOnClickListener {

            if (!allowUserForUsage() && timerLockView.visibility == View.GONE){
                timerLockView.visibility = View.VISIBLE
            }
            else if (allowUserForUsage() && timerLockView.visibility == View.VISIBLE){
                timerLockView.visibility = View.GONE
            }

            val hideAnimation = AnimationHelper.scaleToHideAnimation()
            hideAnimation.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    menuPreviewContainer.visibility = View.GONE
                }

                override fun onAnimationStart(p0: Animation?) {
                }

            })
            menuPreviewContainer.startAnimation(hideAnimation)

            val animation = AnimationHelper.inFromRightAnimation()
            animation.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {
                    topMenuContainer.visibility = View.VISIBLE
                }

                override fun onAnimationEnd(p0: Animation?) {
                }

            })
            topMenuContainer.startAnimation(animation)
        }

        openPermissionSettings.setOnClickListener {
            Utility.openPermissionSettings(this@MainActivity)
        }

//        screenRotateContainer.setOnClickListener {
//            if (mPreviewFragment != null)
//                mPreviewFragment!!.isTextureRotated = true
//        }
        /*
        Tapsell.setRewardListener { ad, completed ->
            if (ad.isRewardedAd && completed)
                mIsAddRewardAvailable = true
        }
        */

        val appKey = "016ebac4ba1f4fe6d7f061776915f6569576fff930c5eb61"
        Appodeal.initialize(this, appKey, Appodeal.INTERSTITIAL)
    }

    fun bindPreviewFragment(){
        if (PermissionUtil.shouldAskPermission()) {
            PermissionUtil.checkPermission(this, this@MainActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, object : PermissionUtil.PermissionAskListener {
                override fun onNeedPermission() {
                    val PERMISSIONS = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    ActivityCompat.requestPermissions(this@MainActivity as Activity, PERMISSIONS, REQUEST_CAMERA_PERMISSION)
                }

                override fun onPermissionPreviouslyDenied() {
                    permissionView.visibility = View.VISIBLE
                }

                override fun onPermissionDisabled() {
                    permissionView.visibility = View.VISIBLE
                }

                override fun onPermissionGranted() {
                    mPreviewFragment = PreviewFragment()
                    // Begin the transaction
                    val ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.previewFragmentContainer, mPreviewFragment!!)
                    ft.commitAllowingStateLoss()
                    permissionView.visibility = View.GONE
                }

            })
        }else{
            mPreviewFragment = PreviewFragment()
            // Begin the transaction
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.previewFragmentContainer, mPreviewFragment!!)
            ft.commitAllowingStateLoss()
            permissionView.visibility = View.GONE
        }
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle?) {
        if (mPreviewFragment != null) {
            val cameraType = if (mPreviewFragment!!.mCurrentCameraType == CameraRenderer.CameraType.Back) {
                1
            } else {
                2
            }

            if (savedInstanceState != null) {
                savedInstanceState.putInt("camera_type", cameraType)
                savedInstanceState.putInt("current_filter", mPreviewFragment!!.mCurrentFilterId)
            }
        }

        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        Log.d("onRestoreInstanceState","Called")

        if (savedInstanceState != null && mPreviewFragment != null){
            val cameraType = savedInstanceState.getInt("camera_type")
            if (cameraType == 1){
                mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Back
            }
            else if (cameraType == 2) {
                mPreviewFragment!!.mCurrentCameraType = CameraRenderer.CameraType.Front
            }

            mPreviewFragment!!.mCurrentFilterId = savedInstanceState.getInt("current_filter")
        }

        super.onRestoreInstanceState(savedInstanceState)
    }

    fun handleCapturingProcess(){
        if (mPreviewFragment != null) {
            mPreviewFragment!!.startSavingPicture(object : PreviewFragment.OnPictureSavedListener {
                override fun onPictureSaved(uri: Uri?, path: String) {
                    captureBtn.isEnabled = true
                    endLoadingAnimation()
                    Picasso.with(this@MainActivity)
                            .load(uri)
                            .placeholder(R.drawable.image_place_holder)
                            .error(R.drawable.image_place_holder)
                            .into(lastCapturedImageView)
                    overridePendingTransition(R.anim.slide_in_right_a,R.anim.slide_out_right_a)
                    val editor = mSettings.edit()
                    editor.putString(getString(R.string.images_last_path_key), "file://" + path)
                    editor.apply()
                    EditImageActivity.start(this@MainActivity,path,path+"_edited",PHOTO_EDITOR_REQUEST,true)
                }
            })
            handleShutterEffect()
            startLoadingAnimation()
        }
    }

    fun handleShutterEffect(){
        Utility.playShutterSound(this@MainActivity)

        if (shutterCurtainView != null){
//            shutterCurtainView.visibility = View.VISIBLE
//            Handler().postDelayed({
//                shutterCurtainView.visibility = View.GONE
//            },300)

            val animation = AnimationHelper.fadeOutAnimation()
            animation.setAnimationListener(object :Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    shutterCurtainView.visibility = View.GONE
                    shutterCurtainView.clearAnimation()
                }

                override fun onAnimationStart(p0: Animation?) {
                    shutterCurtainView.visibility = View.VISIBLE
                }
            })
            shutterCurtainView.startAnimation(animation)
        }
    }

    fun getBackgroundFrame() : Bitmap{
        backgroundFrame.buildDrawingCache()
        return Bitmap.createBitmap(backgroundFrame.drawingCache)
    }

    fun hideSecondaryMenu(){
        val height = Utility.slideDown(secondaryMenuContainer)
        if (filterValueSelectorContainer.visibility == View.VISIBLE){
            Utility.slideDownByHeight(filterValueSelectorContainer,height)
        }

        if (mCurrentMenuType == MenuType.Filters){
            filterRecyclerView.recycledViewPool.clear()
            filterRecyclerView.adapter = null
            System.gc()
        }

        if (filterValueSelectorContainer.visibility == View.VISIBLE)
            filterValueSelectorContainer.visibility = View.GONE

        mCurrentMenuType = MenuType.Unknown
        secondaryMenuContainer.visibility = View.GONE

//        filterRecyclerView.visibility = View.GONE
//        aspectRatioRecyclerView.visibility = View.GONE
    }

    fun showSecondaryMenu(menuType: MenuType){
        mCurrentMenuType = menuType
        val height = Utility.slideUp(secondaryMenuContainer)
        if (filterValueSelectorContainer.visibility == View.VISIBLE){
            Utility.slideUpByHeight(filterValueSelectorContainer,height)
        }
        filterRecyclerView.visibility = View.GONE
        aspectRatioRecyclerView.visibility = View.GONE
        frameCategoryContainer.visibility = View.GONE
        if (menuType == MenuType.Filters){
            bottomMenuTitle.text = getString(R.string.bottom_menu_filter_title)
            filterRecyclerView.visibility = View.VISIBLE
            filterRecyclerView.setHasFixedSize(true)
            filterRecyclerView.setItemViewCacheSize(12)
            filterRecyclerView.isDrawingCacheEnabled = true
            val filterAdapter = FiltersAdapter(this@MainActivity, !allowUserForUsage())
            filterAdapter.setFilterListener(this)
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            filterRecyclerView.layoutManager = layoutManager
            filterRecyclerView.adapter = filterAdapter
        }
        else if (menuType == MenuType.AspectRatio){
            bottomMenuTitle.text = getString(R.string.bottom_menu_aspect_title)
            aspectRatioRecyclerView.visibility = View.VISIBLE
            var isFrontCamera = false
            if (mPreviewFragment != null)
                isFrontCamera = mPreviewFragment!!.mCurrentCameraType != CameraRenderer.CameraType.Back
            val aspectsAdapter = AspectRatioAdapter(this@MainActivity,!allowUserForUsage(),isFrontCamera)
            aspectsAdapter.setAspectRatioSelectionListener(this)
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            aspectRatioRecyclerView.layoutManager = layoutManager
            aspectRatioRecyclerView.adapter = aspectsAdapter
        }

        else if (menuType == MenuType.FrameCategories){
            bottomMenuTitle.text = getString(R.string.bottom_menu_frame_title)
            frameCategoryContainer.visibility = View.VISIBLE
            framesBackBtn.visibility = View.GONE
            val categoriesAdapter = FrameCategoryAdapter(this@MainActivity,!allowUserForUsage())
            categoriesAdapter.setFrameCategorySelectionListener(this@MainActivity)
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            frameCategoryRatioRecyclerView.layoutManager = layoutManager
            frameCategoryRatioRecyclerView.adapter = categoriesAdapter
        }

    }

    fun showLicenceDialog(){
        val paymentDialog = PaymentDialog()
        paymentDialog.isCancelable = false
        paymentDialog.show(supportFragmentManager,"PaymentDialog")
    }

    fun enableCapturing(){
//        if (captureLockView != null && captureLockView.visibility == View.VISIBLE) {
//            captureLockView.visibility = View.GONE
//            demoTextView.visibility = View.GONE
            isCapturingEnabled = true
//            captureBtn.visibility = View.VISIBLE
//        }
    }

    fun disableCapturing(){
//        if (captureLockView != null && captureLockView.visibility == View.GONE) {
//            captureLockView.visibility = View.VISIBLE
            demoTextView.visibility = View.VISIBLE
            isCapturingEnabled = false
//            captureBtn.visibility = View.GONE
//        }
    }

    override fun onFrameCategorySelected(index: Int, categoryTitle: String?) {
        if (index == 0){
            // select without frame option
            frameImageView.visibility = View.GONE
            hideSecondaryMenu()
        }
        else {
            // show frames items
            bottomMenuTitle.text = categoryTitle
            framesBackBtn.visibility = View.VISIBLE
            val frameType = when (index) {
                1 -> FramesAdapter.FrameTypes.Black
                2 -> FramesAdapter.FrameTypes.Colourful
                3 -> FramesAdapter.FrameTypes.Love
                else -> FramesAdapter.FrameTypes.Other
            }

            val framesAdapter = FramesAdapter(this@MainActivity,!allowUserForUsage(),frameType)
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            frameCategoryRatioRecyclerView.layoutManager = layoutManager
            frameCategoryRatioRecyclerView.adapter = framesAdapter

            framesAdapter.setFrameItemSelectionListener { index ->
                if (allowUserForUsage() || frameType == FramesAdapter.FrameTypes.Black) {
                    val selectedFrameResourceId = framesAdapter.getFrameResourceId(index)
                    if (selectedFrameResourceId != -1) {
                        frameImageView.visibility = View.VISIBLE
                        Picasso.with(this@MainActivity)
                                .load(selectedFrameResourceId)
                                .noFade()
                                .noPlaceholder()
                                .into(frameImageView)
                    }
                    hideSecondaryMenu()
                }else{
                    showLicenceDialog()
                }
            }
        }
    }

    override fun onFilterSelected(filterIndex: Int,isActiveFilter:Boolean,isUsable:Boolean) {
        if (mPreviewFragment!= null) {
            if (isUsable) {
                LogHelper.logFilterUsage(filterIndex)
                if (mPreviewFragment!!.setSelectedFilter(filterIndex)!!) {
                    // filter is valuable
                    filterValueSelectorContainer.visibility = View.VISIBLE
//                    filterValueSeekBar.visibility = View.VISIBLE
                    initFilterSeekbarWithProperValue()
                } else {
                    filterValueSelectorContainer.visibility = View.GONE
//                    filterValueSeekBar.visibility = View.GONE
                }

                if (!isActiveFilter)
                    disableCapturing()
                else
                    enableCapturing()
            } else {
                showLicenceDialog()
            }
        }
    }

    override fun onAspectRatioSelected(index: Int) {
        if (index < 2 || allowUserForUsage()) {
            if (mCurrentAspectRatio != index) {
                mCurrentAspectRatio = index
                if (index == 0) {
                    val constraintSet = ConstraintSet()
                    changeUIColor(false)
                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0)
                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, 0)
                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0)
                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
//                constraintSet.constrainHeight(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
//                constraintSet.constrainWidth(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
                    constraintSet.applyTo(container)
                }
                else if (index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6) {
                    // specific aspect ratio
                    when (index) {
                        1 -> configureSpecificAspectRatio("1:1")
                        2 -> configureSpecificAspectRatio("4:3")
                        3 -> configureSpecificAspectRatio("3:2")
                        4 -> configureSpecificAspectRatio("16:9")
                        5 -> configureSpecificAspectRatio("9:16")
                        6 -> configureSpecificAspectRatio("2:3")
                    }
                } else if (index == 7) {
                    showCustomAspectRatioDialog()
                }
//                else if (index == 1) {
//                    // story mode
//                    val constraintSet = ConstraintSet()
//                    changeUIColor(true)
//                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0)
//                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, 0)
//                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0)
//                    constraintSet.connect(R.id.backgroundFrame, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
////                constraintSet.constrainHeight(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
////                constraintSet.constrainWidth(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
//                    constraintSet.applyTo(container)
//                }
            }
        } else{
            showLicenceDialog()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CAMERA_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    bindPreviewFragment()
                }
            }
        }
    }

    fun configureSpecificAspectRatio(aspectRatio:String){
        Log.d("ChangingAspectRatio",aspectRatio)
        changeUIColor(false)
        val constraintSet = ConstraintSet()
        constraintSet.connect(R.id.backgroundFrame, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0)
        constraintSet.connect(R.id.backgroundFrame, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT, 0)
        constraintSet.connect(R.id.backgroundFrame, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0)
        constraintSet.connect(R.id.backgroundFrame, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
//        constraintSet.constrainHeight(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
//        constraintSet.constrainWidth(R.id.backgroundFrame, ConstraintSet.MATCH_CONSTRAINT)
        constraintSet.setDimensionRatio(R.id.backgroundFrame,aspectRatio)
        constraintSet.applyTo(container)
        hideSecondaryMenu()
    }

    fun showCustomAspectRatioDialog(){
        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        val customView = layoutInflater.inflate(R.layout.custom_aspect_dialog, null)
        dialog.setContentView(customView)
        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window.setGravity(Gravity.CENTER)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)
        dialog.show()

        customView.customAspectDialogDoneBtn.setOnClickListener {
            var width = if (!customView.aspectRatioWidthEditText.text.toString().isEmpty()){
                customView.aspectRatioWidthEditText.text.toString()
            }
            else{
                "4"
            }


            var height = if (!customView.aspectRatioHeightEditText.text.toString().isEmpty()){
                customView.aspectRatioHeightEditText.text.toString()
            }
            else{
                "3"
            }

            val aspectRatio = getString(R.string.aspect_ratio_format,width,height)
            configureSpecificAspectRatio(aspectRatio)
            dialog.dismiss()
        }

        customView.customAspectDialogCancelBtn.setOnClickListener {
            dialog.dismiss()
        }
    }

    fun toggleSecondMenu(menuType: MenuType){
        if (mCurrentMenuType != menuType)
            showSecondaryMenu(menuType)
        else
            hideSecondaryMenu()
    }

    fun toggleGridLines(){
        var resourceId = 0
        if (gridLinesImage.visibility == View.GONE){
            gridLinesImage.visibility = View.VISIBLE
            resourceId = R.drawable.ic_grid_on
        }
        else{
            gridLinesImage.visibility = View.GONE
            resourceId = R.drawable.ic_grid_off
        }

        val outAnimation = AnimationHelper.outToBottomAnimation()
        outAnimation.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                gridBtn.setImageResource(resourceId)
                gridBtn.startAnimation(AnimationHelper.inFromTopAnimation())
            }

            override fun onAnimationStart(p0: Animation?) {

            }

        })
        gridBtn.startAnimation(outAnimation)
    }

    fun toggleTimer(){
        if (allowUserForUsage()) {
            var resourceId = 0
            when (mCurrentTimerType) {
                TimerType.OFF -> {
                    mCurrentTimerType = TimerType.Three
                    resourceId = R.drawable.ic_timer_3
                }
                TimerType.Three -> {
                    mCurrentTimerType = TimerType.Ten
                    resourceId = R.drawable.ic_timer_10
                }
                TimerType.Ten -> {
                    mCurrentTimerType = TimerType.OFF
                    resourceId = R.drawable.ic_timer_off
                }
            }

            val outAnimation = AnimationHelper.outToBottomAnimation()
            outAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationEnd(p0: Animation?) {
                    timerBtn.setImageResource(resourceId)
                    timerBtn.startAnimation(AnimationHelper.inFromTopAnimation())
                }

                override fun onAnimationStart(p0: Animation?) {

                }

            })
            timerBtn.startAnimation(outAnimation)
        }
        else
            showLicenceDialog()

    }

    fun changeUIColor(isTransparent:Boolean){
        var colorId  = if (isTransparent){
            R.color.colorPrimaryTransparent
        } else{
            R.color.colorPrimary
        }

        secondaryMenuContainer.setBackgroundColor(ContextCompat.getColor(this@MainActivity,colorId))
        bottomMenu.setBackgroundColor(ContextCompat.getColor(this@MainActivity,colorId))
    }

    fun getFrameBitmap(): Bitmap? {
        return if (frameImageView.visibility == View.GONE)
            null
        else{

            frameImageView.buildDrawingCache()
            Bitmap.createBitmap(frameImageView.drawingCache)
        }
    }

    fun animateDisplayTextView(text:String){
        if (displayTextView.text.toString() != text) {
            displayTextView.text = text
            var animation = AnimationHelper.displayScaleAnimation()
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationEnd(p0: Animation?) {
                    displayTextView.visibility = View.GONE
                }

                override fun onAnimationStart(p0: Animation?) {
                    displayTextView.visibility = View.VISIBLE
                }

            })
            displayTextView.startAnimation(animation)
        }
    }

    fun initFilterSeekbarWithProperValue(){
        try {
            if (mSelectedFilterValues.containsKey(mPreviewFragment!!.mCurrentFilterId)) {
                filterValueSelector.position = (mSelectedFilterValues[mPreviewFragment!!.mCurrentFilterId]!!).toFloat() / 10
            } else {
                filterValueSelector.position = 0F

//                filterValueSeekBar.progress = 0
            }
            filterValueSelector.invalidate()
        } catch (exception:Exception){
            exception.printStackTrace()
        }
    }

    override fun onSurfaceSwipeLeft() {
        if (mPreviewFragment != null) {
            if (mPreviewFragment!!.mCurrentFilterId < 9 || allowUserForUsage()) {
                if (mPreviewFragment!!.showNextFilter()!!) {
                    // filter is valuable
                    filterValueSelectorContainer.visibility = View.VISIBLE
                    initFilterSeekbarWithProperValue()
                } else {
                    filterValueSelectorContainer.visibility = View.GONE
                }
                animateDisplayTextView(mPreviewFragment!!.getCurrentFilterTitle())

                if (mPreviewFragment!!.mCurrentFilterId > 2 && !allowUserForUsage())
                    disableCapturing()
                else
                    enableCapturing()
            } else {
                showLicenceDialog()
            }
        }
    }

    override fun onSurfaceSwipeRight() {
        if (mPreviewFragment != null) {
            if (mPreviewFragment!!.showPrevFilter()!!) {
                // filter is valuable
                filterValueSelectorContainer.visibility = View.VISIBLE
                initFilterSeekbarWithProperValue()
            } else {
                filterValueSelectorContainer.visibility = View.GONE
            }
            if (mPreviewFragment!!.mCurrentFilterId > 2 && !allowUserForUsage())
                disableCapturing()
            else
                enableCapturing()

            animateDisplayTextView(mPreviewFragment!!.getCurrentFilterTitle())
        }
    }

    override fun onSurfaceClicked() {
        if (menuPreviewContainer.visibility == View.GONE) {
            val showAnimation = AnimationHelper.scaleToShowAnimation()
            showAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {
                    menuPreviewContainer.visibility = View.VISIBLE
                }

            })
            menuPreviewContainer.startAnimation(showAnimation)

            val animation = AnimationHelper.outToLeftAnimation()
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {

                }

                override fun onAnimationEnd(p0: Animation?) {
                    topMenuContainer.visibility = View.GONE
                }

            })
            topMenuContainer.startAnimation(animation)
        }
        if (mCurrentMenuType != MenuType.Unknown){
            hideSecondaryMenu()
        }


    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("MainActivity", "onActivityResult($requestCode,$resultCode,$data")

//        // Pass on the activity result to the helper for handling
//        if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data)
//        } else {
//            Log.d("MainActivity", "onActivityResult handled by IABUtil.")
//        }

        if (requestCode == PHOTO_EDITOR_REQUEST){
            try {
                val newFilePath = data!!.getStringExtra(EditImageActivity.EXTRA_OUTPUT)
                val editor = mSettings.edit()
                editor.putString(getString(R.string.images_last_path_key), newFilePath)
                editor.apply()
            } catch (e:Exception){

            }
        }
    }

    override fun onApplicationUnlocked() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle(getString(R.string.payment_success_dialog_title))
        builder.setMessage(getString(R.string.payment_success_dialog_desc))
        val dialog = builder.create()
        dialog.show()

        hideSecondaryMenu()
//        if (captureLockView.visibility == View.VISIBLE){
//            captureLockView.visibility = View.GONE
//        }
        if (timerLockView.visibility == View.VISIBLE){
            timerLockView.visibility = View.GONE
        }
        if (!isCapturingEnabled){
            enableCapturing()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
//        PurchaseHelper.destroyHelper()
        mLoadingAnimationHandler.removeCallbacks(runnable)
        filterRecyclerView.adapter = null
        frameCategoryRatioRecyclerView.adapter = null
        aspectRatioRecyclerView.adapter = null
    }

    var animationProgress = 0f
    var isReverseAnimation = false

    val mLoadingAnimationHandler = Handler()

    private val runnable :Runnable = Runnable {
        if (!isReverseAnimation)
            animationProgress+=2
        else
            animationProgress-=2

        if (animationProgress == 100f)
            isReverseAnimation = true
        else if (animationProgress == 0f)
            isReverseAnimation = false
        val pullProgress = animationProgress/100f
        lastCaptureRefreshView.pullProgress(0f, pullProgress)
        startLoadingAnimation()
    }


    fun startLoadingAnimation(){
        if (lastCaptureRefreshView != null && lastCaptureRefreshView.visibility == View.GONE)
            lastCaptureRefreshView.visibility = View.VISIBLE
        mLoadingAnimationHandler.postDelayed(runnable,5)
    }

    fun endLoadingAnimation(){
        mLoadingAnimationHandler.removeCallbacks(runnable)
        animationProgress = 0f
        isReverseAnimation = false
        if (lastCaptureRefreshView != null && lastCaptureRefreshView.visibility == View.VISIBLE)
            lastCaptureRefreshView.visibility = View.GONE
    }

    fun handleAdReward(){
        mIsAddRewardAvailable = true
        hideSecondaryMenu()
        topMenuContainer.visibility = View.GONE
    }

    fun allowUserForUsage() : Boolean{
        return mIsAddRewardAvailable || isApplicationLocked()
    }

    private val ONE_SECOND_IN_MS = 1000

    private var mTimerCountDown : CountDownTimer? = null

    fun startTimerCountDown(){

        // TODO : use handler here for creating time loop
        timerView.visibility = View.VISIBLE
        val time = when (mCurrentTimerType) {
            TimerType.Three -> 3
            TimerType.Ten -> 10
            else -> 0
        }

        timerView.setMax(time)
        mTimerCountDown =  object : CountDownTimer((time*ONE_SECOND_IN_MS).toLong(), ONE_SECOND_IN_MS.toLong()) {
            override fun onFinish() {
                timerView.setProgress(0)
                Utility.playShutterSound(this@MainActivity)
                timerView.visibility = View.GONE
                handleCapturingProcess()
            }

            override fun onTick(millisUntilFinished: Long) {
                val secondsRemaining = (millisUntilFinished / ONE_SECOND_IN_MS).toInt()
                timerView.setProgress(secondsRemaining)
                Utility.playShutterSound(this@MainActivity)
            }
        }
        mTimerCountDown!!.start()
    }

    fun showRotateScreenOption(){
        if (screenRotateContainer != null && screenRotateContainer.visibility == View.GONE){
            val animation = AnimationHelper.inFromRightAnimation()
            animation.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationEnd(p0: Animation?) {
                    Handler().postDelayed({
                        screenRotateContainer.clearAnimation()
                        val hideAnimation = AnimationHelper.outToRightAnimation()
                        hideAnimation.setAnimationListener(object:Animation.AnimationListener{
                            override fun onAnimationRepeat(p0: Animation?) {

                            }

                            override fun onAnimationEnd(p0: Animation?) {
                                screenRotateContainer.visibility = View.GONE
                            }

                            override fun onAnimationStart(p0: Animation?) {

                            }

                        })
                        screenRotateContainer.startAnimation(hideAnimation)

                    },6000)
                }

                override fun onAnimationStart(p0: Animation?) {
                    screenRotateContainer.visibility = View.VISIBLE
                }

            })
            screenRotateContainer.startAnimation(animation)
        }
    }

    override fun onResume() {
        super.onResume()
        bindPreviewFragment()
    }

    override fun onPause() {
        super.onPause()
        try {
            if (mTimerCountDown != null)
                mTimerCountDown!!.cancel()
            if (timerView != null)
                timerView.visibility = View.GONE
            mPreviewFragment?.stopPreview()
            mPreviewFragment?.clearMemory()
            supportFragmentManager.beginTransaction().remove(mPreviewFragment!!).commitAllowingStateLoss()
            mPreviewFragment = null
            System.gc()
        } catch (e:Exception){

        }
    }

    override fun onBackPressed() {
        if (secondaryMenuContainer.visibility == View.VISIBLE){
            hideSecondaryMenu()
        }else
            super.onBackPressed()
    }
}
