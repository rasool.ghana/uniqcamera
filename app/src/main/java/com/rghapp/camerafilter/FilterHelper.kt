package com.rghapp.camerafilter

/**
 * Created by Rasool Ghana on 12/12/17.
 * Email : Rasool.ghana@gmail.com
 */
class FilterHelper{
    companion object {

        fun getFilterResourceId(position:Int) :Int{
            when (position) {
                0 -> return R.drawable.if_original
                1 -> return R.drawable.if_mirror
                2 -> return R.drawable.if_pinch
                3 -> return R.drawable.if_wave
                4 -> return R.drawable.if_buldge
                5 -> return R.drawable.if_circle
                6 -> return R.drawable.if_fish_eye
                7 -> return R.drawable.if_tile_mosaic
                8 -> return R.drawable.if_legofied
                9 -> return R.drawable.if_original
                10 -> return R.drawable.if_triangle_mosaic
                11 -> return R.drawable.if_plastic_bw
                12 -> return R.drawable.if_duplicate
                13 -> return R.drawable.if_money
                14 -> return R.drawable.if_edge_detection
                15 -> return R.drawable.if_mapping
                16 -> return R.drawable.if_blueorange
                17 -> return R.drawable.if_lichtenstein
                18 -> return R.drawable.if_em_interface
                19 -> return R.drawable.if_pixelize
                20 -> return R.drawable.if_rotate
                21 -> return R.drawable.if_contrast
                22 -> return R.drawable.if_bw
                23 -> return R.drawable.if_line
                24 -> return R.drawable.if_tv_noise
                25 -> return R.drawable.if_tv
                26 -> return R.drawable.if_original
                27 -> return R.drawable.if_4in1_th
                28 -> return R.drawable.if_asciart
                29 -> return R.drawable.if_hm
                30 -> return R.drawable.if_chromatic
                31 -> return R.drawable.if_original
                32 -> return R.drawable.if_crosshatch
                33 -> return R.drawable.if_filter1
                34 -> return R.drawable.if_filter2
                35 -> return R.drawable.if_filter3
                36 -> return R.drawable.if_filter4
                37 -> return R.drawable.if_filter5
                38 -> return R.drawable.if_filter6
                39 -> return R.drawable.if_original
                40 -> return R.drawable.if_refraction
                41 -> return R.drawable.if_original
                else -> {
                    return -1
                }
            }
        }
    }
}