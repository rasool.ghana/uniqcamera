package com.rghapp.camerafilter

import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.GestureDetectorCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.rghapp.instatext.Utility
import kotlinx.android.synthetic.main.preview_fragment.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*








/**
 * Created by Rasool Ghana on 12/13/17.
 * Email : Rasool.ghana@gmail.com
 */
class PreviewFragment : Fragment(), View.OnTouchListener,CameraRenderer2.CameraRendererListener {
    override fun onRendererDestroyed() {
//        if (cameraLoadingContainer != null && cameraLoadingContainer.visibility == View.GONE)
//            cameraLoadingContainer.visibility = View.VISIBLE
//        clearMemory()
    }

    override fun onStartingRender() {
        if (cameraLoadingContainer != null && cameraLoadingContainer.visibility == View.VISIBLE)
            cameraLoadingContainer.visibility = View.GONE
        mLoadingAnimationHandler.removeCallbacks(runnable)
        animationProgress = 0f
        isReverseAnimation = false
//        if (mCurrentCameraType == CameraRenderer.CameraType.Front){
//            (activity as MainActivity).showRotateScreenOption()
//        }
    }

    private var detector: GestureDetectorCompat? = null

    companion object {
        fun newInstance(filterId:Int,cameraType:Int): PreviewFragment {
            val f = PreviewFragment()
            val args = Bundle()
            args.putInt("filter_id", filterId)
            args.putInt("camera_type", cameraType)
            f.arguments = args
            return f
        }
    }

    private var mRenderer: CameraRenderer2? = null
    var isTextureRotated = false
    lateinit var mSurfaceClickListener : OnSurfaceClickListener
    var mCurrentFilterId : Int = 0
    var mCurrentCameraType : CameraRenderer.CameraType = CameraRenderer.CameraType.Back
    lateinit var mFilterTitles : Array<String>

    var animationProgress = 0f
    var isReverseAnimation = false

    val mLoadingAnimationHandler = Handler()

    fun clearMemory(){
        mRenderer = null

        mFilterTitles = emptyArray()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.preview_fragment,container,false)
        if (arguments != null){
            mCurrentFilterId = arguments!!.getInt("filter_id",0)
            val cameraType = arguments!!.getInt("camera_type",1)
            if (cameraType == 1){
                mCurrentCameraType = CameraRenderer.CameraType.Back
            }
            else if (cameraType == 2){
                mCurrentCameraType = CameraRenderer.CameraType.Front
            }
        }
        return view
    }

    private val runnable :Runnable = Runnable {
        if (!isReverseAnimation)
            animationProgress+=2
        else
            animationProgress-=2

        if (animationProgress == 100f)
            isReverseAnimation = true
        else if (animationProgress == 0f)
            isReverseAnimation = false
        val pullProgress = animationProgress/100f
        shoot_refresh_view.pullProgress(0f, pullProgress)
        startLoadingAnimation()
    }


    fun startLoadingAnimation(){
        mLoadingAnimationHandler.postDelayed(runnable,5)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        startLoadingAnimation()



        mFilterTitles = resources.getStringArray(R.array.filter_names)

        detector = GestureDetectorCompat(context, onSwipeListener)
        mSurfaceClickListener = activity as MainActivity

//        if (ContextCompat.checkSelfPermission(activity!!,
//                Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.CAMERA)) {
//                Toast.makeText(activity, "Camera access is required.", Toast.LENGTH_SHORT).show()
//
//            } else {
//                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.CAMERA),
//                        MainActivity.REQUEST_CAMERA_PERMISSION)
//            }
//
//        } else {
            setupCameraPreviewView()
//        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDetach() {
        mLoadingAnimationHandler.removeCallbacks(runnable)
        super.onDetach()
    }


    fun setupCameraPreviewView() {
        mRenderer = CameraRenderer2(activity,textureView,this@PreviewFragment)
//        if (mCurrentCameraType == CameraRenderer.CameraType.Front) {
//            if (!isTextureRotated) {
//                isTextureRotated = true
//                mRenderer?.isTextureRotated = false
//            } else {
//                mRenderer?.isTextureRotated = true
//            }
//        }
        mRenderer?.setCameraType(mCurrentCameraType)
        mRenderer?.setSelectedFilter(mCurrentFilterId)
        textureView.surfaceTextureListener = mRenderer

        // Show original frame when touch the view
//        textureView.setOnTouchListener { v, event ->
//            when (event.action) {
////                MotionEvent.ACTION_DOWN -> mRenderer?.setSelectedFilter(0)
//                MotionEvent.ACTION_DOWN -> mSurfaceClickListener.onSurfaceClicked()
//
//                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> mRenderer?.setSelectedFilter(mCurrentFilterId)
//            }
//            true
//        }
        textureView.setOnTouchListener(this)

        textureView.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            mRenderer!!.onSurfaceTextureSizeChanged(null, v.width, v.height)
        }
    }

    fun stopPreview(){
        mRenderer?.stopPreview()
    }

    fun setSelectedFilter(filterIndex: Int) : Boolean?{
        mCurrentFilterId = filterIndex
        return mRenderer?.setSelectedFilter(mCurrentFilterId)
    }

    fun changeSelectedFilterValue(valueIndex : Int){
        mRenderer?.changeSelectedFilterValue(valueIndex)
    }



    fun caputre(): Boolean {
        val mPath = genSaveFileName("CameraFilter" + "_", ".png")
        val imageFile = File(mPath)
        if (imageFile.exists()) {
            imageFile.delete()
        }

        // create bitmap screen capture
        val bitmap = textureView.bitmap
        var outputStream: OutputStream? = null

        try {
            outputStream = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream)
            outputStream!!.flush()
            outputStream!!.close()

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            return false
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }

        return true
    }

    fun startSavingPicture(listener:OnPictureSavedListener){
        SaveTask(listener).execute()
    }

    private fun captureImage() : Bitmap?{
        if (textureView != null) {
            val cameraBitmap = textureView.bitmap

            Log.d("TextureBitmap", "Height = ${cameraBitmap.height} && Width = ${cameraBitmap.width}")
            Log.d("DisplayMetric", "Height = ${resources.displayMetrics.heightPixels} && Width = ${resources.displayMetrics.widthPixels}")

            // rotate if picture taken by front camera

            val rotatedCameraBitmap = if (mCurrentCameraType == CameraRenderer.CameraType.Front)
                Utility.RotateBitmap(cameraBitmap, 180F, true)
            else
                cameraBitmap

            val frameBitmap: Bitmap? = (activity as MainActivity).getFrameBitmap()
            return if (frameBitmap != null) {
                Utility.overlay(rotatedCameraBitmap, frameBitmap)
            } else
                rotatedCameraBitmap
        }else{
            return null
        }
    }

    private inner class SaveTask(private val mListener: OnPictureSavedListener?) : AsyncTask<Void, Void, Void>() {
        private val mHandler: Handler = Handler()

        override fun doInBackground(vararg params: Void): Void? {
            val result = captureImage()
            if (result != null) {
                saveImage(getString(R.string.images_folder_name), generatePureFileName(), result)
            }
            return null
        }


        private fun saveImage(folderName: String, fileName: String, image: Bitmap) {
            val path = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val file = File(path, folderName + "/" + fileName)
            try {
                file.parentFile.mkdirs()
                image.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(file))

                try {
                    MediaScannerConnection.scanFile(activity,
                            arrayOf(file.toString()), null
                    ) { path, uri ->
                        if (mListener != null) {
                            mHandler.post {
                                mListener.onPictureSaved(uri,path)
                            }

                        }
                    }
                } catch (e:Exception){
                    try {
                        if (mListener != null) {
                            mHandler.post {
                                mListener.onPictureSaved(Uri.fromFile(file),file.path)
                            }
                        }
                    } catch (e:Exception){

                    }

                }


            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

        }
    }

    interface OnPictureSavedListener {
        fun onPictureSaved(uri: Uri?, path:String)
    }

    interface OnSurfaceClickListener{
        fun onSurfaceClicked()
        fun onSurfaceSwipeLeft()
        fun onSurfaceSwipeRight()
    }

    fun generatePureFileName():String{
        val date = Date()
        val dateformat1 = SimpleDateFormat("yyyyMMdd_hhmmss")
        val timeString = dateformat1.format(date)
        return getString(R.string.images_name_format,timeString)
    }



    private fun genSaveFileName(prefix: String, suffix: String): String {
        val date = Date()
        val dateformat1 = SimpleDateFormat("yyyyMMdd_hhmmss")
        val timeString = dateformat1.format(date)
        val externalPath = Environment.getExternalStorageDirectory().toString()
        return externalPath + "/" + prefix + timeString + suffix
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        mSurfaceClickListener.onSurfaceClicked()
        detector!!.onTouchEvent(motionEvent)
        return true
    }

    fun showNextFilter():Boolean?{
        if (mCurrentFilterId < 45) {
            mCurrentFilterId++
            return mRenderer?.setSelectedFilter(mCurrentFilterId)
        }
        return false
    }

    fun showPrevFilter():Boolean?{
        if (mCurrentFilterId > 0) {
            mCurrentFilterId--
            return mRenderer?.setSelectedFilter(mCurrentFilterId)
        }
        return false
    }

    fun getCurrentFilterTitle():String = mFilterTitles[mCurrentFilterId]

    var onSwipeListener: OnSwipeListener = object : OnSwipeListener() {

        override fun onSwipe(direction: Direction): Boolean {


            if (direction === Direction.left) {
                mSurfaceClickListener.onSurfaceSwipeLeft()
                return true
            }else if (direction === Direction.right) {
                mSurfaceClickListener.onSurfaceSwipeRight()
                return true
            }

            return super.onSwipe(direction)
        }
    }

}