package com.rghapp.camerafilter;

/**
 * Created by Rasool Ghana on 11/23/17.
 * Email : Rasool.ghana@gmail.com
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rghapp.instatext.Utility;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jeffrey Liu on 3/21/16.
 */
public class FiltersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Context mContext;

    private String[] mFilterTitles;

    private FilterSelectionListener mListener;

    private boolean isLocked = true;
    private int imageSize;

    public FiltersAdapter(Context context,boolean isLocked) {
        mFilterTitles = context.getResources().getStringArray(R.array.filter_names);
        this.mContext = context;
        this.isLocked = isLocked;
        imageSize = (int) (context.getResources().getDisplayMetrics().density * 75);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null){
            CellViewHolder viewHolder = (CellViewHolder) view.getTag();
            if (!isLocked || viewHolder.getAdapterPosition() <=2) {
                mListener.onFilterSelected(viewHolder.getAdapterPosition(),true,true);
            }
            else if (viewHolder.getAdapterPosition() > 2 && viewHolder.getAdapterPosition() <= 9) {
                mListener.onFilterSelected(viewHolder.getAdapterPosition(),false,true);
            }
            else if (viewHolder.getAdapterPosition() > 9 ){
                mListener.onFilterSelected(viewHolder.getAdapterPosition(),false,false);
            }
        }
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView mFilterImageView;
        private TextView mFilterTitle;
        private View mFilterContainer;
        private View mLockView;

        public CellViewHolder(View itemView) {
            super(itemView);
            mFilterTitle = itemView.findViewById(R.id.filterTitle);
            mFilterImageView = itemView.findViewById(R.id.filterImageView);
            mFilterContainer = itemView.findViewById(R.id.filterRoot);
            mLockView = itemView.findViewById(R.id.lockView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_image_cell, viewGroup, false);
                CellViewHolder viewHolder = new CellViewHolder(v1);
                viewHolder.mFilterContainer.setOnClickListener(FiltersAdapter.this);
                return viewHolder;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                final CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.mFilterContainer.setTag(cellViewHolder);
                cellViewHolder.mFilterTitle.setText(mFilterTitles[position]);
                int resourceId = FilterHelper.Companion.getFilterResourceId(position);
                if (resourceId != -1) {
                    Picasso.with(mContext)
                            .load(resourceId)
                            .noPlaceholder()
                            .noFade()
                            .resize(imageSize,imageSize)
                            .into(cellViewHolder.mFilterImageView);
                    cellViewHolder.mFilterImageView.setBorderColor(Utility.Companion.generateRandomMaterialColor());
                }
                if (isLocked && position > 2) {
                    cellViewHolder.mLockView.setVisibility(View.VISIBLE);
                }
                else {
                    cellViewHolder.mLockView.setVisibility(View.GONE);
                }
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mFilterTitles.length;
    }

    public interface FilterSelectionListener{
        void onFilterSelected(int filterIndex,boolean isActiveFilter,boolean isUsable);
    }

    public void setFilterListener(FilterSelectionListener filterListener){
        this.mListener = filterListener;
    }

}