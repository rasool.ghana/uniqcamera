package com.rghapp.camerafilter

import android.view.animation.*



/**
 * Created by Rasool Ghana on 12/17/17.
 * Email : Rasool.ghana@gmail.com
 */
class AnimationHelper{
    

    
    companion object {
        val ANIMATION_DURATION = 200L

        fun scaleToHideAnimation() : AnimationSet{
            val animation = AnimationSet(true)
            animation.addAnimation(AlphaAnimation(1.0f, 0.0f))
            animation.addAnimation(ScaleAnimation(1f, 0f, 1f, 0f))
            animation.duration = ANIMATION_DURATION
            return animation
        }

        fun fadeOutAnimation() : Animation{
            val animation = AlphaAnimation(1.0f, 0.0f)
            animation.duration = 300
            animation.interpolator = AccelerateInterpolator()
            return animation
        }

        fun scaleToShowAnimation() : AnimationSet{
            val animation = AnimationSet(true)
            animation.addAnimation(AlphaAnimation(0.0f, 1.0f))
            animation.addAnimation(ScaleAnimation(0f, 1f, 0f, 1f))
            animation.duration = ANIMATION_DURATION
            return animation
        }

        fun displayScaleAnimation() : AnimationSet{
            val animation = AnimationSet(true)
            animation.addAnimation(AlphaAnimation(1.0f, 0.0f))
//            animation.addAnimation(ScaleAnimation(1f, 1.4f, 0f, 1.4f,
//                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f))
            animation.duration = 1000
            return animation
        }

         fun inFromRightAnimation(): Animation {

            val inFromRight = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, +1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f)
            inFromRight.duration = ANIMATION_DURATION
            inFromRight.interpolator = AccelerateInterpolator()
            return inFromRight
        }

        fun inFromTopAnimation(): Animation {
            val inFromTop = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, -1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f)
            inFromTop.duration = 75
            inFromTop.fillAfter = true
            inFromTop.interpolator = AccelerateInterpolator()
            return inFromTop
        }

        fun outToBottomAnimation(): Animation {
            val outToBottom = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, +1.0f)
            outToBottom.duration = 75
            outToBottom.fillAfter = true
            outToBottom.interpolator = AccelerateInterpolator()
            return outToBottom
        }

         fun outToLeftAnimation(): Animation {
            val outtoLeft = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, -1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f)
            outtoLeft.duration = ANIMATION_DURATION
            outtoLeft.interpolator = AccelerateInterpolator()
            return outtoLeft
        }

         fun inFromLeftAnimation(): Animation {
            val inFromLeft = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, -1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f)
            inFromLeft.duration = ANIMATION_DURATION
            inFromLeft.interpolator = AccelerateInterpolator()
            return inFromLeft
        }

        fun outToRightAnimation(): Animation {
            val outtoRight = TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, +1.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.0f)
            outtoRight.duration = 300L
            outtoRight.interpolator = AccelerateInterpolator()
            return outtoRight
        }

    }
}