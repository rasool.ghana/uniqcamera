package com.rghapp.camerafilter

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ir.tapsell.sdk.*
import kotlinx.android.synthetic.main.payment_dialog.*




/**
 * Created by Rasool Ghana on 12/22/17.
 * Email : Rasool.ghana@gmail.com
 */
class PaymentDialog : BottomSheetDialogFragment(){

    var mTapsellAd : TapsellAd? = null
    val mShowOptions : TapsellShowOptions? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.payment_dialog,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paymentBtn.setOnClickListener {
            dismiss()
            PurchaseHelper.startBuyProcess(activity!!,activity as MainActivity)
        }

        val mShowOptions = TapsellShowOptions()
        mShowOptions.isBackDisabled = true
        mShowOptions.isShowDialog = true

        advertiseButton.setOnClickListener {
            if (mTapsellAd != null){
                if (mTapsellAd!!.isShown){
                    requestForNewAdvertise(true)
                }else
                    mTapsellAd!!.show(activity,mShowOptions)
            }
            dismiss()
        }
        requestForNewAdvertise(false)
    }

    fun requestForNewAdvertise(playAfter:Boolean){
        val options = TapsellAdRequestOptions()
        options.cacheType = TapsellAdRequestOptions.CACHE_TYPE_STREAMED
        Tapsell.requestAd(activity, "5bb636bd2a9fee000123b93d", options, object : TapsellAdRequestListener {
            override fun onError(error: String) {}

            override fun onAdAvailable(ad: TapsellAd) {
                mTapsellAd = ad
                if (playAfter)
                    mTapsellAd!!.show(activity,mShowOptions)
            }

            override fun onNoAdAvailable() {}

            override fun onNoNetwork() {}

            override fun onExpiring(ad: TapsellAd) {}
        })
    }

}