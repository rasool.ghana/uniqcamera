package com.rghapp.camerafilter

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * Created by Rasool Ghana on 9/29/18.
 * Email : Rasool.ghana@gmail.com
 */
class LogHelper {

    companion object {

        private var mFirebaseAnalytics : FirebaseAnalytics? = null

        fun initLogger(context: Context){
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)
        }

        fun logFilterUsage(filterIndex : Int){
            val bundle = Bundle()
            bundle.putInt("selected_filter_type",filterIndex)
            mFirebaseAnalytics!!.logEvent("app_filter_usage", bundle)
        }
    }
}