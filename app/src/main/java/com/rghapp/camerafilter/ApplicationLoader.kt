package com.rghapp.camerafilter

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.onesignal.OneSignal
import io.fabric.sdk.android.Fabric
import uk.co.chrisjenx.calligraphy.CalligraphyConfig




/**
 * Created by rasool on 3/14/16.
 */
class ApplicationLoader : Application() {

    @SuppressLint("ApplySharedPref")
    override fun onCreate() {
        super.onCreate()
//        ApplicationLoader.applicationContext = applicationContext

//        multiDexInit()

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSansMobile.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        )

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        Fabric.with(this, Crashlytics())

        LogHelper.initLogger(applicationContext)


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        multiDexInit()

        // Tapsell.initialize(this, "bptbinhirhcmqnarspparohjljhtmchloipscajeoaeefinmdnddtfoqhillpbqsfahajm");


    }

    fun multiDexInit(){
        try {
            MultiDex.install(this)

        } catch (multiDexException: RuntimeException) {
            // Work around Robolectric causing multi dex installation to fail, see
            // https://code.google.com/p/android/issues/detail?id=82007.
            var isUnderUnitTest: Boolean

            try {
                val robolectric = Class.forName("org.robolectric.Robolectric")
                isUnderUnitTest = robolectric != null
            } catch (e: ClassNotFoundException) {
                isUnderUnitTest = false
            }

            if (!isUnderUnitTest) {
                // Re-throw if this does not seem to be triggered by Robolectric.
//                Crashlytics.logException(multiDexException)
                throw multiDexException
            }
        }

    }

    companion object {
        lateinit var applicationContext: Context
    }
}
