package com.rghapp.camerafilter

import android.annotation.SuppressLint
import android.app.Activity
import android.preference.PreferenceManager
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.rghapp.inapp.IabHelper


/**
 * Created by Rasool Ghana on 12/21/17.
 * Email : Rasool.ghana@gmail.com
 */
@SuppressLint("StaticFieldLeak")
class PurchaseHelper {

    interface LicenceListener{
        fun onApplicationUnlocked()
    }

    companion object {
        var mHelper: IabHelper? = null
        var base64EncodedPublicKey = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCVbjFBe9Wag4xL7mP8b6eMqNDmiRpCDqzr6+Lk8UAK9ZpPTnLpYSmxCt+99tPBu" +
                "YzigYXZHAR1MkiN4jkegLHLM6L0tb7Vu6xUOh/bOPqOT" +
                "WxWkE0SU7Zm1F1OTseAXfscPS7/3da3P8YHUqfoGJzbTd/0R7h+254uhK" +
                "4bHWiBTPwEVSWN/m6HcZCmHgWwVrQlk44fLHHj2L" +
                "DiHtxWjxljxuASNUISJDu4ZHktznECAwEAAQ=="

        val SKU_PREMIUM = "licence"

        val TAG = "PurchaseHelper"
        var mIsPremium = false
        val RC_REQUEST = 365
        var isPaymentSupported = true

        lateinit var mActivity : Activity
        lateinit var mListener : LicenceListener

        val mGotInventoryListener = IabHelper.QueryInventoryFinishedListener { result, inventory ->
            Log.d(TAG, "Query inventory finished.")
            if (result.isFailure) {
                Log.d(TAG, "Failed to query inventory: " + result)
                try{
                    // if cant fetch result from server try to load licence status from settings
                    mIsPremium = PreferenceManager.getDefaultSharedPreferences(mActivity).
                            getBoolean(mActivity.getString(R.string.pref_licence),false)

                } catch (e:Exception){
                    e.printStackTrace()
                }
                return@QueryInventoryFinishedListener
            } else {
                Log.d(TAG, "Query inventory was successful.")
                // does the user have the premium upgrade?
                mIsPremium = inventory.hasPurchase(SKU_PREMIUM)
                // update UI accordingly

                Log.d(TAG, "User is " + if (mIsPremium) "PREMIUM" else "NOT PREMIUM")
            }

            Log.d(TAG, "Initial inventory query finished; enabling main UI.")
        }

        @SuppressLint("ApplySharedPref")
        val mPurchaseFinishedListener = IabHelper.OnIabPurchaseFinishedListener { result, purchase ->
            if (result.isFailure) {
                Log.d(TAG, "Error purchasing: " + result)
                return@OnIabPurchaseFinishedListener
            } else if (purchase.sku == SKU_PREMIUM) {
                // give user access to premium content and update the UI
                mIsPremium = true
                mListener.onApplicationUnlocked()
                try {
                    PreferenceManager.getDefaultSharedPreferences(mActivity).edit()
                            .putBoolean(mActivity.getString(R.string.pref_licence), mIsPremium).commit()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
        }

        fun init(activity: Activity){
            mHelper = IabHelper(activity, base64EncodedPublicKey)
            mActivity = activity

            Log.d(TAG, "Starting setup.")

            try {
                mHelper!!.startSetup(IabHelper.OnIabSetupFinishedListener { result ->
                    Log.d(TAG, "Setup finished.")

                    if (!result.isSuccess) {
                        // Oh noes, there was a problem.
                        Log.d(TAG, "Problem setting up In-app Billing: " + result)
                    }
                    else // Hooray, IAB is fully set up!
                        mHelper!!.queryInventoryAsync(mGotInventoryListener)

                })
            } catch (e:Exception){
                Crashlytics.logException(e)
                isPaymentSupported = false
                e.printStackTrace()
            }

        }

        fun startBuyProcess(activity: Activity,listener:LicenceListener){
            if (mHelper != null && isPaymentSupported) {
                mListener = listener
                try {
                    mHelper!!.launchPurchaseFlow(activity, SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener, "payload-string")
                } catch (e:Exception){
                }
            }
        }

        fun destroyHelper(){
            try {
                if (mHelper != null)
                    mHelper!!.dispose()
                mHelper = null
            } catch (e:Exception){}
        }

        fun isApplicationLocked():Boolean = false
    }
}