/*
 * Copyright 2016 nekocode
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rghapp.camerafilter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.Surface;
import android.view.TextureView;

import com.rghapp.camerafilter.filter.AsciiArtFilter;
import com.rghapp.camerafilter.filter.BasicDeformFilter;
import com.rghapp.camerafilter.filter.BlackWhiteFilter;
import com.rghapp.camerafilter.filter.BlueorangeFilter;
import com.rghapp.camerafilter.filter.BuldgeFilter;
import com.rghapp.camerafilter.filter.CameraFilter;
import com.rghapp.camerafilter.filter.ChromaticAberrationFilter;
import com.rghapp.camerafilter.filter.ContrastFilter;
import com.rghapp.camerafilter.filter.CrackedFilter;
import com.rghapp.camerafilter.filter.CrosshatchFilter;
import com.rghapp.camerafilter.filter.DuplicateFilter;
import com.rghapp.camerafilter.filter.EMInterferenceFilter;
import com.rghapp.camerafilter.filter.EdgeDetectionFilter;
import com.rghapp.camerafilter.filter.Filter1Filter;
import com.rghapp.camerafilter.filter.Filter2Filter;
import com.rghapp.camerafilter.filter.Filter3Filter;
import com.rghapp.camerafilter.filter.Filter4Filter;
import com.rghapp.camerafilter.filter.Filter5Filter;
import com.rghapp.camerafilter.filter.Filter6Filter;
import com.rghapp.camerafilter.filter.FourInOneFilter;
import com.rghapp.camerafilter.filter.HalfMirror;
import com.rghapp.camerafilter.filter.JFAVoronoiFilter;
import com.rghapp.camerafilter.filter.LegofiedFilter;
import com.rghapp.camerafilter.filter.LichtensteinEsqueFilter;
import com.rghapp.camerafilter.filter.LineFilter;
import com.rghapp.camerafilter.filter.MappingFilter;
import com.rghapp.camerafilter.filter.MoneyFilter;
import com.rghapp.camerafilter.filter.NoiseWarpFilter;
import com.rghapp.camerafilter.filter.OriginalFilter;
import com.rghapp.camerafilter.filter.PaintFilter;
import com.rghapp.camerafilter.filter.PinchFilter;
import com.rghapp.camerafilter.filter.PixelizeFilter;
import com.rghapp.camerafilter.filter.PlasticBWFilter;
import com.rghapp.camerafilter.filter.PlasticFilter;
import com.rghapp.camerafilter.filter.PolygonizationFilter;
import com.rghapp.camerafilter.filter.RefractionFilter;
import com.rghapp.camerafilter.filter.RotateFilter;
import com.rghapp.camerafilter.filter.SobelFilter;
import com.rghapp.camerafilter.filter.TVFilter;
import com.rghapp.camerafilter.filter.TVNoiseFilter;
import com.rghapp.camerafilter.filter.TileMosaicFilter;
import com.rghapp.camerafilter.filter.TrianglesMosaicFilter;
import com.rghapp.camerafilter.filter.WaveFilter;

import java.io.IOException;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;


/**
 * @author nekocode (nekocode.cn@gmail.com)
 */
public class CameraRenderer implements Runnable, TextureView.SurfaceTextureListener {
    private static final String TAG = "CameraRenderer";
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
    private static final int DRAW_INTERVAL = 1000 / 30;

    private Thread renderThread;
    private Context context;
    private SurfaceTexture surfaceTexture;
    private int gwidth, gheight;

    private EGLDisplay eglDisplay;
    private EGLSurface eglSurface;
    private EGLContext eglContext;
    private EGL10 egl10;

    private Camera camera;
    private SurfaceTexture cameraSurfaceTexture;
    private SurfaceTexture cameraFrontSurfaceTexture;
    private int cameraTextureId;
    private CameraFilter selectedFilter;
    private int selectedFilterId = 0;
    private SparseArray<CameraFilter> cameraFilterMap = new SparseArray<>();
    private CameraType mCameraType = CameraType.Back;
    private TextureView mTextureView;

    public enum CameraType {
        Back,Front
    }

    public CameraRenderer(Context context, TextureView textureView) {
        this.context = context;
        this.mTextureView = textureView;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        gwidth = -width;
        gheight = -height;

    }



    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        stopPreview();


        return true;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (renderThread != null && renderThread.isAlive()) {
            renderThread.interrupt();
        }
        renderThread = new Thread(this);

        surfaceTexture = surface;
        gwidth = -width;
        gheight = -height;

        try {
            // Open camera
            if (mCameraType == CameraType.Back) {
                Pair<Camera.CameraInfo, Integer> backCamera = getBackCamera();
                final int backCameraId = backCamera.second;
                camera = Camera.open(backCameraId);
                setCameraDisplayOrientation(backCamera.first);
            }
            else {
                Pair<Camera.CameraInfo, Integer> frontCamera = getFrontCamera();
                final int frontCameraId = frontCamera.second;
                camera = Camera.open(frontCameraId);
                setCameraDisplayOrientation(frontCamera.first);

                //create a matrix to invert the x-plane
                Matrix matrix = new Matrix();
                matrix.setScale(1, -1);
                //move it back to in view otherwise it'll be off to the left.
                matrix.postTranslate(0, height);
                mTextureView.setTransform(matrix);

            }
        } catch (Exception e){
            e.printStackTrace();
        }

        // Start rendering
        renderThread.start();
    }

    public void setSelectedFilter(int id) {
        selectedFilterId = id;
        selectedFilter = cameraFilterMap.get(id);
        if (selectedFilter != null)
            selectedFilter.onAttach();
    }

    private void setCameraDisplayOrientation(android.hardware.Camera.CameraInfo info) {
        int rotation = ((Activity)context).getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }


    boolean firstInit = true;

    @Override
    public void run() {
//
//        try {
//            surfaceTexture.detachFromGLContext();
//        } catch (Exception e){
//            e.printStackTrace();
//        }

        try {

            initGL(surfaceTexture);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (firstInit) {
            firstInit = false;



            // Setup camera filters map
            cameraFilterMap.append(0, new OriginalFilter(context));
            cameraFilterMap.append(1, new MappingFilter(context));
            cameraFilterMap.append(2, new TileMosaicFilter(context));
            cameraFilterMap.append(3, new LegofiedFilter(context));
            cameraFilterMap.append(4, new MoneyFilter(context));
            cameraFilterMap.append(5, new EdgeDetectionFilter(context));
            cameraFilterMap.append(6, new PixelizeFilter(context));
            cameraFilterMap.append(7, new LichtensteinEsqueFilter(context));
            cameraFilterMap.append(8, new JFAVoronoiFilter(context));
            cameraFilterMap.append(9, new RefractionFilter(context));
            cameraFilterMap.append(10, new PolygonizationFilter(context));
            cameraFilterMap.append(11, new NoiseWarpFilter(context));
            cameraFilterMap.append(12, new EMInterferenceFilter(context));
            cameraFilterMap.append(13, new TrianglesMosaicFilter(context));
            cameraFilterMap.append(14, new BlueorangeFilter(context));
            cameraFilterMap.append(15, new ChromaticAberrationFilter(context));
            cameraFilterMap.append(16, new BasicDeformFilter(context));
            cameraFilterMap.append(17, new ContrastFilter(context));
            cameraFilterMap.append(18, new CrosshatchFilter(context));
            cameraFilterMap.append(19, new AsciiArtFilter(context));
            cameraFilterMap.append(20, new CrackedFilter(context));
            cameraFilterMap.append(21, new BlackWhiteFilter(context));
            cameraFilterMap.append(22, new PlasticFilter(context));
            cameraFilterMap.append(23, new PlasticBWFilter(context));
            cameraFilterMap.append(24, new PaintFilter(context));
            cameraFilterMap.append(25, new RotateFilter(context));
            cameraFilterMap.append(26, new PinchFilter(context));
            cameraFilterMap.append(27, new WaveFilter(context));
            cameraFilterMap.append(28, new BuldgeFilter(context));
            cameraFilterMap.append(29, new LineFilter(context));
            cameraFilterMap.append(30, new TVNoiseFilter(context));
            cameraFilterMap.append(31, new TVFilter(context));
            cameraFilterMap.append(32, new SobelFilter(context));
            cameraFilterMap.append(33, new DuplicateFilter(context));
            cameraFilterMap.append(34, new FourInOneFilter(context));
            cameraFilterMap.append(35, new HalfMirror(context));
            cameraFilterMap.append(36, new Filter1Filter(context));
            cameraFilterMap.append(37, new Filter2Filter(context));
            cameraFilterMap.append(38, new Filter3Filter(context));
            cameraFilterMap.append(39, new Filter4Filter(context));
            cameraFilterMap.append(40, new Filter5Filter(context));
            cameraFilterMap.append(41, new Filter6Filter(context));
        }


//
//

        setSelectedFilter(selectedFilterId);

        // Create texture for camera preview
        cameraTextureId = MyGLUtils.genTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
        cameraSurfaceTexture = new SurfaceTexture(cameraTextureId);

        // Start camera preview
        try {
            if (camera != null){
                camera.setPreviewTexture(cameraSurfaceTexture);
            }


        } catch (IOException ioe) {
            // Something bad happened
        }


        if (camera != null )
            camera.startPreview();



        // Render loop
        while (!Thread.currentThread().isInterrupted()) {
            try {
                if (gwidth < 0 && gheight < 0)
                    GLES20.glViewport(0, 0, gwidth = -gwidth, gheight = -gheight);

                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

                // Update the camera preview texture
                synchronized (this) {
                    cameraSurfaceTexture.updateTexImage();
                }

                // Draw camera preview
                selectedFilter.draw(cameraTextureId, gwidth, gheight);

                // Flush
                GLES20.glFlush();
                egl10.eglSwapBuffers(eglDisplay, eglSurface);

                Thread.sleep(DRAW_INTERVAL);

//                Log.d("RenderLoop","Active");

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        if (cameraSurfaceTexture!= null) {
//            cameraSurfaceTexture.detachFromGLContext();
            cameraSurfaceTexture.release();
            GLES20.glDeleteTextures(1, new int[]{cameraTextureId}, 0);
            cameraSurfaceTexture = null;
        }
//
//        // Start rendering
//        renderThread.start();
    }


    public void toggleCamera(){

        if (mCameraType == CameraType.Back)
            mCameraType = CameraType.Front;
        else if (mCameraType == CameraType.Front)
            mCameraType = CameraType.Back;

        stopPreview();


        renderThread = new Thread(this);



        try {
            // Open camera
            if (mCameraType == CameraType.Back) {
                Pair<Camera.CameraInfo, Integer> backCamera = getBackCamera();
                final int backCameraId = backCamera.second;
                camera = Camera.open(backCameraId);
            }
            else {
                Pair<Camera.CameraInfo, Integer> frontCamera = getFrontCamera();
                final int frontCameraId = frontCamera.second;
                camera = Camera.open(frontCameraId);
                //create a matrix to invert the x-plane
                Matrix matrix = new Matrix();
                matrix.setScale(1, -1);
                //move it back to in view otherwise it'll be off to the left.
                matrix.postTranslate(0, gheight * -1);
                mTextureView.setTransform(matrix);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        // Start rendering
        renderThread.run();

    }


    public void setCameraType(CameraType cameraType) {
        this.mCameraType = cameraType;
    }

    public void stopPreview(){

        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }

        if (renderThread != null && renderThread.isAlive()) {
            renderThread.interrupt();
            Log.d("RenderLoop","Interrupted");
        }

        if (cameraSurfaceTexture != null) {
//            cameraSurfaceTexture.detachFromGLContext();
            cameraSurfaceTexture.release();
            GLES20.glDeleteTextures(1, new int[]{cameraTextureId}, 0);
            cameraSurfaceTexture = null;
        }

        CameraFilter.release();
    }


    public int getCorrectCameraOrientation(Camera.CameraInfo info, Camera camera, AppCompatActivity activity) {

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch(rotation){
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;

        }

        int result;
        if(info.facing==Camera.CameraInfo.CAMERA_FACING_FRONT){
            result = (info.orientation+ degrees) % 360;
            result = (360-result) % 360;
        }else{
            result = (info.orientation- degrees + 360) % 360;
        }

        return result;
    }

    private void initGL(SurfaceTexture texture) {
        if (firstInit)
            egl10 = (EGL10) EGLContext.getEGL();

        eglDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (eglDisplay == EGL10.EGL_NO_DISPLAY) {
            throw new RuntimeException("eglGetDisplay failed " +
                    android.opengl.GLUtils.getEGLErrorString(egl10.eglGetError()));
        }

        int[] version = new int[2];
        if (!egl10.eglInitialize(eglDisplay, version)) {
            throw new RuntimeException("eglInitialize failed " +
                    android.opengl.GLUtils.getEGLErrorString(egl10.eglGetError()));
        }

        int[] configsCount = new int[1];
        EGLConfig[] configs = new EGLConfig[1];
        int[] configSpec = {
                EGL10.EGL_RENDERABLE_TYPE,
                EGL_OPENGL_ES2_BIT,
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 8,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE
        };

        EGLConfig eglConfig = null;
        if (!egl10.eglChooseConfig(eglDisplay, configSpec, configs, 1, configsCount)) {
            throw new IllegalArgumentException("eglChooseConfig failed " +
                    android.opengl.GLUtils.getEGLErrorString(egl10.eglGetError()));
        } else if (configsCount[0] > 0) {
            eglConfig = configs[0];
        }
        if (eglConfig == null) {
            throw new RuntimeException("eglConfig not initialized");
        }

        int[] attrib_list = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE};
        eglContext = egl10.eglCreateContext(eglDisplay, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);

        if (firstInit) {
            eglSurface = egl10.eglCreateWindowSurface(eglDisplay, eglConfig, texture, null);

            if (eglSurface == null || eglSurface == EGL10.EGL_NO_SURFACE) {
                int error = egl10.eglGetError();
                if (error == EGL10.EGL_BAD_NATIVE_WINDOW) {
                    Log.e(TAG, "eglCreateWindowSurface returned EGL10.EGL_BAD_NATIVE_WINDOW");
                    return;
                }
                throw new RuntimeException("eglCreateWindowSurface failed " +
                        android.opengl.GLUtils.getEGLErrorString(error));
            }
        }

        if (!egl10.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext)) {
            throw new RuntimeException("eglMakeCurrent failed " +
                    android.opengl.GLUtils.getEGLErrorString(egl10.eglGetError()));
        }
    }

    private Pair<Camera.CameraInfo, Integer> getBackCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        final int numberOfCameras = Camera.getNumberOfCameras();

        for (int i = 0; i < numberOfCameras; ++i) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return new Pair<>(cameraInfo, i);
            }
        }
        return null;
    }

    private Pair<Camera.CameraInfo, Integer> getFrontCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        final int numberOfCameras = Camera.getNumberOfCameras();

        for (int i = 0; i < numberOfCameras; ++i) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return new Pair<>(cameraInfo, i);
            }
        }
        return null;
    }
}