package com.rghapp.camerafilter;

/**
 * Created by Rasool Ghana on 11/23/17.
 * Email : Rasool.ghana@gmail.com
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Jeffrey Liu on 3/21/16.
 */
public class AspectRatioAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Context mContext;

    private String[] mAspectsTitle;

    private AspectRatioListener mListener;

    private boolean isLocked = true;

    private boolean isFrontCamera = false;

    private SparseIntArray mImages;

    public AspectRatioAdapter(Context context, boolean isLocked,boolean isFrontCamera) {
        this.mContext = context;
        this.isLocked = isLocked;
        this.isFrontCamera = isFrontCamera;
        if (!isFrontCamera)
            mAspectsTitle = context.getResources().getStringArray(R.array.aspect_ratio_back_sizes);
        else
            mAspectsTitle = new String[]{"عادی"};

        mImages = new SparseIntArray();
        mImages.put(0,R.drawable.ic_frame_normal);
        mImages.put(1,R.drawable.ic_frame_story);
        mImages.put(2,R.drawable.ic_frame_square);
        mImages.put(3,R.drawable.ic_frame_horizontal);
        mImages.put(4,R.drawable.ic_frame_horizontal);
        mImages.put(5,R.drawable.ic_frame_horizontal);
        mImages.put(6,R.drawable.ic_frame_vertical);
        mImages.put(7,R.drawable.ic_frame_vertical);
        mImages.put(8,R.drawable.ic_frame_custom);
    }

    public void switchCamera(){
        if (!isFrontCamera)
            mAspectsTitle = new String[]{"عادی"};
        else
            mAspectsTitle = mContext.getResources().getStringArray(R.array.aspect_ratio_back_sizes);
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (mListener != null){
            CellViewHolder viewHolder = (CellViewHolder) view.getTag();
            mListener.onAspectRatioSelected(viewHolder.getAdapterPosition());
        }
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView mFilterImageView;
        private TextView mFilterTitle;
        private View mFilterContainer;
        private View mLockView;

        public CellViewHolder(View itemView) {
            super(itemView);
            mFilterTitle = itemView.findViewById(R.id.aspectRatioTitle);
            mFilterImageView = itemView.findViewById(R.id.aspectRatioImageView);
            mFilterContainer = itemView.findViewById(R.id.aspectRoot);
            mLockView = itemView.findViewById(R.id.lockView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.aspect_ratio_cell, viewGroup, false);
                CellViewHolder viewHolder = new CellViewHolder(v1);
                viewHolder.mFilterContainer.setOnClickListener(AspectRatioAdapter.this);
                return viewHolder;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                final CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.mFilterContainer.setTag(cellViewHolder);
                cellViewHolder.mFilterTitle.setText(mAspectsTitle[position]);
                cellViewHolder.mFilterImageView.setImageResource(mImages.get(position));

                if (isLocked && position > 1)
                    cellViewHolder.mLockView.setVisibility(View.VISIBLE);
                else
                    cellViewHolder.mLockView.setVisibility(View.GONE);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAspectsTitle.length;
    }

    public interface AspectRatioListener{
        void onAspectRatioSelected(int index);
    }

    public void setAspectRatioSelectionListener(AspectRatioListener aspectRatioListener){
        this.mListener = aspectRatioListener;
    }

}