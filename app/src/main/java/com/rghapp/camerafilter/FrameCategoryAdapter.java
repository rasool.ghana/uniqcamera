package com.rghapp.camerafilter;

/**
 * Created by Rasool Ghana on 11/23/17.
 * Email : Rasool.ghana@gmail.com
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Jeffrey Liu on 3/21/16.
 */
public class FrameCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Context mContext;

    private String[] mFrameCategoryTitle;

    private FrameCategoryListener mListener;

    private boolean isLocked = true;


    private SparseIntArray mCategoriesImage;

    public FrameCategoryAdapter(Context context, boolean isLocked) {
        this.mContext = context;
        this.isLocked = isLocked;
        mFrameCategoryTitle = context.getResources().getStringArray(R.array.frame_categories_title);
        mCategoriesImage = new SparseIntArray();
        mCategoriesImage.put(0,R.drawable.if_original);
        mCategoriesImage.put(1,R.drawable.frame_black12);
        mCategoriesImage.put(2,R.drawable.frame_color2);
        mCategoriesImage.put(3,R.drawable.frame_love5);
        mCategoriesImage.put(4,R.drawable.frame_special2);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null){
            CellViewHolder viewHolder = (CellViewHolder) view.getTag();
            int pos = viewHolder.getAdapterPosition();
            mListener.onFrameCategorySelected(pos,mFrameCategoryTitle[pos]);
        }
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {

        private ImageView mFilterImageView;
        private TextView mFilterTitle;
        private View mFilterContainer;
        private View mLockView;

        public CellViewHolder(View itemView) {
            super(itemView);
            mFilterTitle = itemView.findViewById(R.id.frameTitleTextView);
            mFilterImageView = itemView.findViewById(R.id.frameCellImageView);
            mFilterContainer = itemView.findViewById(R.id.frameRoot);
            mLockView = itemView.findViewById(R.id.lockView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frame_category_cell, viewGroup, false);
                CellViewHolder viewHolder = new CellViewHolder(v1);
                viewHolder.mFilterContainer.setOnClickListener(FrameCategoryAdapter.this);
                return viewHolder;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                final CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.mFilterContainer.setTag(cellViewHolder);
                cellViewHolder.mFilterTitle.setText(mFrameCategoryTitle[position]);
                Picasso.with(mContext)
                        .load(mCategoriesImage.get(position))
                        .noPlaceholder()
                        .into(cellViewHolder.mFilterImageView);

                if (isLocked && position > 1)
                    cellViewHolder.mLockView.setVisibility(View.VISIBLE);
                else
                    cellViewHolder.mLockView.setVisibility(View.GONE);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mFrameCategoryTitle.length;
    }

    public interface FrameCategoryListener{
        void onFrameCategorySelected(int index,String categoryTitle);
    }

    public void setFrameCategorySelectionListener(FrameCategoryListener frameCategoryListener){
        this.mListener = frameCategoryListener;
    }

}