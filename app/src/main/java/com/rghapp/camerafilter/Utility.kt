package com.rghapp.instatext

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.media.AudioManager
import android.media.MediaActionSound
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.animation.*
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import android.widget.EditText
import com.rghapp.camerafilter.BuildConfig
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.net.URL
import java.util.*


/**
 * Created by Rasool Ghana on 10/30/17.
 * Email : Rasool.ghana@gmail.com
 */
class Utility {

    companion object {


        fun getRandomBoolean(): Boolean {
            val random = Random()
            return random.nextBoolean()
        }

        fun getSaltString(): String {

            val saltChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
            val salt = StringBuilder()
            val rnd = Random()
            while (salt.length < 18) { // length of the random string.
                val index = (rnd.nextFloat() * saltChars.length).toInt()
                salt.append(saltChars[index])
            }
            return salt.toString()
        }



        private val MATERIAL_DARK_COLORS = intArrayOf(-0x2cd0d1, // RED
                -0x3de7a5, // PINK
                -0x84e05e, // Purple
                -0xaed258, // Deep Purple
                -0xcfc061, // Indigo
                -0xe6892e, // Blue
                -0xfd772f, // Light Blue
                -0xff6859, // Cyan
                -0xff8695, // Teal
//                -0xc771c4, // Green
//                -0x9760c8, // Light Green
                -0x504bd5, // Lime
                //            0xFFFBC02D, // Yellow
                -0x6000, // Amber
                -0xa8400, // Orange
                -0x19b5e7, // Deep Orange
                -0xa2bfc9, // Brown
                -0x9e9e9f, // Grey
                -0xbaa59c)// Blue Grey

        fun generateRandomMaterialColor(): Int {
            val random = Random()
            return MATERIAL_DARK_COLORS[random.nextInt(0..MATERIAL_DARK_COLORS.size)]
        }

        fun generateRandomNumber(range: IntRange) :Int {
            return Random().nextInt(range)
        }

        private fun Random.nextInt(range: IntRange): Int {
            return range.start + nextInt(range.last - range.start)
        }

        fun RotateBitmap(source: Bitmap, angle: Float,recycle: Boolean): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            val rotatedBitmap = Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
            if (recycle)
                source.recycle()
            return rotatedBitmap
        }

//            public static void shakeView(View view) {
//        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
//        shake.setDuration(300);
//        shake.setInterpolator(new CycleInterpolator(5));
//        view.startAnimation(shake);
//    }

        fun shakeView(view: View){
            val shake = TranslateAnimation(0F,10F,0F,0F);
            shake.duration = 300
            shake.interpolator = CycleInterpolator(5F)
            view.startAnimation(shake)
        }

        fun playShutterSound(context: Context){
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    val audio = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    when (audio.ringerMode) {
                        AudioManager.RINGER_MODE_NORMAL -> {
                            val sound = MediaActionSound()
                            sound.play(MediaActionSound.SHUTTER_CLICK)
                        }
                        AudioManager.RINGER_MODE_SILENT -> {
                        }
                        AudioManager.RINGER_MODE_VIBRATE -> {
                        }
                    }
                }
            } catch (e:Exception){}
        }

        fun openPermissionSettings(context: Context)
        {
            try {
                val intent = Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID))
                context.startActivity(intent)
            } catch (e:Exception){
                e.printStackTrace()
            }
        }
         fun overlay(bmp1: Bitmap, bmp2: Bitmap): Bitmap {
            val bmOverlay = Bitmap.createBitmap(bmp1.width, bmp1.height, bmp1.config)
            val canvas = Canvas(bmOverlay)
            canvas.drawBitmap(bmp1, Matrix(), null)
            canvas.drawBitmap(bmp2, Matrix(), null)
            return bmOverlay
        }


        fun isValidMail(email: String): Boolean =
                android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()

        fun isValidMobile(phone: String): Boolean =
                android.util.Patterns.PHONE.matcher(phone).matches()

        fun sendEmail(email:String,context: Context){
            val i = Intent(Intent.ACTION_SEND)
            i.type = "message/rfc822"
            i.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            try {
                context.startActivity(Intent.createChooser(i, "ارسال ایمیل"))
            } catch (e:Exception){
                e.printStackTrace()
            }
        }

        fun openTelegramAccount(id: String, context: Context) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=" + id))
            context.startActivity(intent)
        }



        fun slideUp(view: View) :Float{
            view.visibility = View.VISIBLE
            val height = view.height.toFloat()
            val animate = TranslateAnimation(
                    0f, // fromXDelta
                    0f, // toXDelta
                    height, // fromYDelta
                    0f)                // toYDelta
            animate.duration = 200
            animate.setAnimationListener(object:Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                }

                override fun onAnimationStart(p0: Animation?) {
                }

            })
            view.startAnimation(animate)
            return height
        }

        fun slideDown(view: View) :Float{
            val height = view.height.toFloat()
            val animate = TranslateAnimation(
                    0f, // fromXDelta
                    0f, // toXDelta
                    0f, // fromYDelta
                    height) // toYDelta
            animate.duration = 200
            animate.setAnimationListener(object: Animation.AnimationListener{
                override fun onAnimationRepeat(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    view.visibility = View.GONE
                    view.clearAnimation()
                }

                override fun onAnimationStart(p0: Animation?) {
                }

            })
            view.startAnimation(animate)
            return height
        }

        fun slideUpByHeight(view:View,height:Float){
            val animate = TranslateAnimation(
                    0f, // fromXDelta
                    0f, // toXDelta
                    height, // fromYDelta
                    0f)                // toYDelta
            animate.duration = 200
            view.startAnimation(animate)
        }

        fun slideDownByHeight(view:View,height:Float){
            val animate = TranslateAnimation(
                    0f, // fromXDelta
                    0f, // toXDelta
                    0f, // fromYDelta
                    height) // toYDelta
            animate.duration = 200

            val alphaAnim = AlphaAnimation(1f,0f)
            alphaAnim.duration = 150
            alphaAnim.fillAfter = true

//            animate.setAnimationListener(object:Animation.AnimationListener{
//                override fun onAnimationRepeat(p0: Animation?) {
//
//                }
//
//                override fun onAnimationEnd(p0: Animation?) {
////                    view.visibility = View.VISIBLE
////                    view.clearAnimation()
//                }
//
//                override fun onAnimationStart(p0: Animation?) {
//                }
//
//            })


            val animationSet = AnimationSet(true)
            animationSet.addAnimation(animate)
            animationSet.addAnimation(alphaAnim)

            view.startAnimation(animationSet)
        }

        fun hideKeyboard(context: Context) {
            val view = (context as Activity).currentFocus
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        fun showKeyboard(editText: EditText, context: Context) {
            editText.requestFocus()
            val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }

        fun getResizedBitmap(bm: Bitmap, newWidth: Int, newHeight: Int,recycle:Boolean): Bitmap {
            val width = bm.width
            val height = bm.height
            val scaleWidth = newWidth.toFloat() / width
            val scaleHeight = newHeight.toFloat() / height
            // CREATE A MATRIX FOR THE MANIPULATION
            val matrix = Matrix()
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight)

            // "RECREATE" THE NEW BITMAP
            val resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false)
            if (recycle)
                bm.recycle()
            return resizedBitmap
        }

        fun isWebUrl(input: CharSequence): Boolean {
            if (TextUtils.isEmpty(input)) {
                return false
            }
            val URL_PATTERN = Patterns.WEB_URL
            var isURL = URL_PATTERN.matcher(input).matches()
            if (!isURL) {
                val urlString = input.toString() + ""
                if (URLUtil.isNetworkUrl(urlString)) {
                    try {
                        URL(urlString)
                        isURL = true
                    } catch (e: Exception) {
                    }

                }
            }
            return isURL
        }

        fun getResId(resName: String, c: Class<*>): Int {
            return try {
                val idField = c.getDeclaredField(resName)
                idField.getInt(idField)
            } catch (e: Exception) {
                e.printStackTrace()
                -1
            }

        }

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.
                    getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun cloneObject(obj: Any): Any? {
            try {
                val clone = obj.javaClass.newInstance()
                for (field in obj.javaClass.declaredFields) {
                    field.isAccessible = true
                    if (field.get(obj) == null || Modifier.isFinal(field.modifiers)) {
                        continue
                    }
                    if (field.type.isPrimitive || field.type == String::class.java
                            || field.type.superclass == Number::class.java
                            || field.type == Boolean::class.java) {
                        field.set(clone, field.get(obj))
                    } else {
                        val childObj = field.get(obj)
                        if (childObj === obj) {
                            field.set(clone, clone)
                        } else {
                            field.set(clone, cloneObject(field.get(obj)))
                        }
                    }
                }
                return clone
            } catch (e: Exception) {
                return null
            }

        }

        fun setLocaleFa(context: Context) {
            val locale = Locale("fa")
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
           context.resources.updateConfiguration(config, null)
        }

        fun setLocaleEn(context: Context) {
            val locale = Locale("en")
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            context.resources.updateConfiguration(config, null)
        }

        @SuppressLint("RestrictedApi")
        fun showStaticBottomNavigationView(bottomNavigationView: BottomNavigationView) {
            try {
                var f: Field? = null
                try {
                    f = bottomNavigationView.javaClass.getDeclaredField("mMenuView")
                } catch (e: NoSuchFieldException) {
                    e.printStackTrace()
                }

                f!!.isAccessible = true
                var menuView: BottomNavigationMenuView? = null
                try {
                    menuView = f.get(bottomNavigationView) as BottomNavigationMenuView
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                }

                //  get the private BottomNavigationItemView[]  field
                try {
                    f = menuView!!.javaClass.getDeclaredField("mButtons")
                } catch (e: NoSuchFieldException) {
                    e.printStackTrace()
                }

                f!!.isAccessible = true
                var mButtons: Array<BottomNavigationItemView>? = null
                try {
                    mButtons = f.get(menuView) as Array<BottomNavigationItemView>
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                }

                try {

                    for (mButton in mButtons!!) {

                        mButton.setChecked(true)
                    }
                } catch (e: Exception) {
                }

                try {
                    f = menuView!!.javaClass.getDeclaredField("mShiftingMode")
                    f!!.isAccessible = true
                    f.setBoolean(menuView, false)
                } catch (e: NoSuchFieldException) {
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                }

            } catch (ignored: Exception) {
            }

        }
    }


    fun fastBlur(sentBitmap: Bitmap, scale: Float, radius: Int): Bitmap? {
        var sentBitmap = sentBitmap

        val width = Math.round(sentBitmap.width * scale)
        val height = Math.round(sentBitmap.height * scale)
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false)

        val bitmap = sentBitmap.copy(sentBitmap.config, true)

        if (radius < 1) {
            return null
        }
        val w = bitmap.width
        val h = bitmap.height
        val pix = IntArray(w * h)
        Log.e("pix", w.toString() + " " + h + " " + pix.size)
        bitmap.getPixels(pix, 0, w, 0, 0, w, h)
        val wm = w - 1
        val hm = h - 1
        val wh = w * h
        val div = radius + radius + 1
        val r = IntArray(wh)
        val g = IntArray(wh)
        val b = IntArray(wh)
        var rsum: Int
        var gsum: Int
        var bsum: Int
        var x: Int
        var y: Int
        var i: Int
        var p: Int
        var yp: Int
        var yi: Int
        var yw: Int
        val vmin = IntArray(Math.max(w, h))
        var divsum = div + 1 shr 1
        divsum *= divsum
        val dv = IntArray(256 * divsum)
        i = 0
        while (i < 256 * divsum) {
            dv[i] = i / divsum
            i++
        }

        yi = 0
        yw = yi

        val stack = Array(div) { IntArray(3) }
        var stackpointer: Int
        var stackstart: Int
        var sir: IntArray
        var rbs: Int
        val r1 = radius + 1
        var routsum: Int
        var goutsum: Int
        var boutsum: Int
        var rinsum: Int
        var ginsum: Int
        var binsum: Int

        y = 0
        while (y < h) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            i = -radius
            while (i <= radius) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))]
                sir = stack[i + radius]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff
                rbs = r1 - Math.abs(i)
                rsum += sir[0] * rbs
                gsum += sir[1] * rbs
                bsum += sir[2] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }
                i++
            }
            stackpointer = radius

            x = 0
            while (x < w) {
                r[yi] = dv[rsum]
                g[yi] = dv[gsum]
                b[yi] = dv[bsum]
                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum
                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]
                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]
                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm)
                }
                p = pix[yw + vmin[x]]
                sir[0] = p and 0xff0000 shr 16
                sir[1] = p and 0x00ff00 shr 8
                sir[2] = p and 0x0000ff

                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]

                rsum += rinsum
                gsum += ginsum
                bsum += binsum

                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer % div]

                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]

                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]

                yi++
                x++
            }
            yw += w
            y++
        }
        x = 0
        while (x < w) {
            bsum = 0
            gsum = bsum
            rsum = gsum
            boutsum = rsum
            goutsum = boutsum
            routsum = goutsum
            binsum = routsum
            ginsum = binsum
            rinsum = ginsum
            yp = -radius * w
            i = -radius
            while (i <= radius) {
                yi = Math.max(0, yp) + x
                sir = stack[i + radius]
                sir[0] = r[yi]
                sir[1] = g[yi]
                sir[2] = b[yi]
                rbs = r1 - Math.abs(i)
                rsum += r[yi] * rbs
                gsum += g[yi] * rbs
                bsum += b[yi] * rbs
                if (i > 0) {
                    rinsum += sir[0]
                    ginsum += sir[1]
                    binsum += sir[2]
                } else {
                    routsum += sir[0]
                    goutsum += sir[1]
                    boutsum += sir[2]
                }

                if (i < hm) {
                    yp += w
                }
                i++
            }
            yi = x
            stackpointer = radius
            y = 0
            while (y < h) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = -0x1000000 and pix[yi] or (dv[rsum] shl 16) or (dv[gsum] shl 8) or dv[bsum]

                rsum -= routsum
                gsum -= goutsum
                bsum -= boutsum

                stackstart = stackpointer - radius + div
                sir = stack[stackstart % div]

                routsum -= sir[0]
                goutsum -= sir[1]
                boutsum -= sir[2]

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w
                }
                p = x + vmin[y]

                sir[0] = r[p]
                sir[1] = g[p]
                sir[2] = b[p]

                rinsum += sir[0]
                ginsum += sir[1]
                binsum += sir[2]

                rsum += rinsum
                gsum += ginsum
                bsum += binsum

                stackpointer = (stackpointer + 1) % div
                sir = stack[stackpointer]

                routsum += sir[0]
                goutsum += sir[1]
                boutsum += sir[2]

                rinsum -= sir[0]
                ginsum -= sir[1]
                binsum -= sir[2]

                yi += w
                y++
            }
            x++
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h)

        return bitmap
    }
}