precision highp float;

uniform vec3                iResolution;
uniform float               iGlobalTime;
uniform sampler2D           iChannel0;
varying vec2                texCoord;

// value of the quad in pixels
const float value = 15.0;

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Radius of the circle
    float radius = value * 0.5;
	// Current quad in pixels
	vec2 quadPos = floor(fragCoord.xy / value) * value;
	// Normalized quad position
	vec2 quad = quadPos/iResolution.xy;
	// Center of the quad
	vec2 quadCenter = (quadPos + value/2.0);
	// Distance to quad center
	float dist = length(quadCenter - fragCoord.xy);

	vec4 texel = texture2D(iChannel0, quad);
	if (dist > radius)
	{
		fragColor = vec4(0.25);
	}
	else
	{
		fragColor = texel;
	}
}


void main() {
	mainImage(gl_FragColor, texCoord*iResolution.xy);
}