precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define SIN01(a) (sin(a)*0.5 + 0.5)
const int value = 11;

float normpdf(in float x, in float sigma)
{
	return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
	vec3 c = texture2D(iChannel0, fragCoord.xy / iResolution.xy).rgb;
        
        
    		const int kSize = (value-1)/2;
    		float kernel[value];
    		vec3 final_colour = vec3(0.0);
        
        float sigma = 7.0;
    		float Z = 0.0;
    		for (int j = 0; j <= kSize; ++j)
    		{
    			kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);
    		}
    		
    		//get the normalization factor (as the gaussian has been clamped)
    		for (int j = 0; j < value; ++j)
    		{
    			Z += kernel[j];
    		}
    		
    		//read out the texels
    		for (int i=-kSize; i <= kSize; ++i)
    		{
    			for (int j=-kSize; j <= kSize; ++j)
    			{
    				final_colour += kernel[kSize+j]*kernel[kSize+i]*texture2D(iChannel0, (fragCoord.xy+vec2(float(i),float(j))) / iResolution.xy).rgb;
    	
    			}
    		}
        
        fragColor = vec4(final_colour/(Z*Z), 1.0);
}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}