precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 0.5

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 pixel = fragCoord.xy / iResolution.xy;
    vec2 adjustedPixel = pixel;
    const float intensity = .1;

    adjustedPixel.y = 0.0;
    adjustedPixel.x = pixel.y;

    vec4 noiseTexture = texture2D( iChannel0,adjustedPixel);
    //vec4 soundTexture = texture2D( iChannel2,adjustedPixel);

    pixel.x += (noiseTexture.x * value) * intensity - .05;
    vec3 videoTexture = texture2D(iChannel0, pixel).xyz;
    //videoTexture.r -= 1.0 - ((sin(iTime * noiseTexture.y) + 1.0) / 2.);

	//videoTexture.g = .2;
	//videoTexture.b = .2;

	fragColor = vec4(videoTexture, 1.0);
}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}