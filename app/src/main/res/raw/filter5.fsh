precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 c = texture2D(iChannel0,uv);
    c = sin(uv.x*10.+c*cos(c*6.28+iGlobalTime+uv.x)*sin(c+uv.y+iGlobalTime)*6.28)*.5+.5;
    c.b+=length(c.rg);
	fragColor = c;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}