precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

//change the number of value
// this is changing value
#define value 6.0

//uncomment below to toggle between light and dark
//#define light

//click and drag the mouse too!

#define tau 6.28

void mainImage( out vec4 fragColor, in vec2 fragCoord ){
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 c  = texture2D(iChannel0,uv);
    float t = 1.0;
    float d = .01+sin(t)*.01+1.0/iResolution.x;
    for(float i = 0.; i<tau;i+=tau/value){
        float a = i+t;
        vec4 c2 = texture2D(iChannel0,vec2(uv.x+cos(a)*d,uv.y+sin(a)*d));
        #ifdef light
        	c = max(c,c2);
        #else
        	c = min(c,c2);
        #endif
    }
	fragColor = c;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}