precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;


float rand(vec2 co)
{
	return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;

	if (mod(iGlobalTime, 2.0) > 1.9)
		uv.x += cos(iGlobalTime * 10.0 + uv.y * 1000.0) * 0.01;

    //if (mod(iGlobalTime, 4.0) > 3.0)
	//	uv = floor(uv * 32.0) / 32.0;

	if (mod(iGlobalTime, 5.0) > 3.75)
    	uv += 1.0 / 64.0 * (2.0 * vec2(rand(floor(uv * 32.0) + vec2(32.05,236.0)), rand(floor(uv.y * 32.0) + vec2(-62.05,-36.0))) - 1.0);

	fragColor = texture2D(iChannel0, uv);

    if (rand(vec2(iGlobalTime)) > 0.90)
		fragColor = vec4(dot(fragColor.rgb, vec3(0.25, 0.5, 0.25)));

    fragColor.rgb += 0.25 * vec3(rand(iGlobalTime + fragCoord / vec2(-213, 5.53)), rand(iGlobalTime - fragCoord / vec2(213, -5.53)), rand(iGlobalTime + fragCoord / vec2(213, 5.53))) - 0.125;
}



void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}