precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;


void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
	vec2 uv = fragCoord.xy / iResolution.xy;
	uv.x = abs(uv.x-.5)+.5;
	vec2 p = uv;
	p.x = uv.y;
	p.y = 1.;
	vec4 t1 = texture2D(iChannel0, p);
	p.x /= 4.0;
	vec4 t2 = texture2D(iChannel0, p);
	t1.y -= (t1.y-0.5) * 0.5;
	t1.x += (t2.y-0.5) * 1.2;
	uv = mod(uv,1.);
	fragColor = texture2D(iChannel0, uv);
}


void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}