precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

// changes in 1.0 step
#define value 10.


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = (fragCoord.xy / iResolution.xy);
    vec4 tex = texture2D(iChannel0, uv);
    float vcol = value*(mod(fragCoord.y/5.,1.)-.45);
	float hCol = value*(mod(fragCoord.x/5.,1.)-.55);
    float gray = hCol;
    if(tex.r>0.5) {
      gray = vcol;
    }
    fragColor = vec4(vec3(gray),1.0);
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}