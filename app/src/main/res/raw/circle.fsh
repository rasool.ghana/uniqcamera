precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 0.40

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 Color = texture2D(iChannel0,uv);
    float dist = distance(uv, vec2(0.5,0.46));
    Color.rgb *= smoothstep(value, 0.38, dist);
	fragColor = Color;
}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}