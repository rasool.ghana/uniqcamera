precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 3

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 c = texture(iChannel0, uv);


    for (int i = 0 ; i < value ; i++){
  c += texture(iChannel0, uv-0.001);
  c += texture(iChannel0, uv-0.003);
  c += texture(iChannel0, uv-0.005);
  c += texture(iChannel0, uv-0.007);
  c += texture(iChannel0, uv-0.009);
  c += texture(iChannel0, uv-0.011);
    }



  c.rgb = vec3((c.r+c.g+c.b)/3.0);
  c = c / 9.5;
  fragColor = c;
}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}