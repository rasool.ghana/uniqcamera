precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

// minimum is 2
#define value 2.0

vec2 mirror(vec2 x)
{
	return abs(fract(x/2.0) - 0.5)*2.0;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv =  value*fragCoord.xy / iResolution.xy;
	fragColor = texture2D(iChannel0, mirror(uv*1.0));
}


void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}