precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define T texture(iChannel0,.5+(p.xy*=.992))
#define value 100.

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec3 temp = vec3(fragCoord.x,fragCoord.y,1.0);
    vec3 p = temp/iResolution-.5;
  	vec3 o = T.rgb;
  	for (float i=0.;i<value;i++)
    	p.z += pow(max(0.,.5-length(T.rg)),2.)*exp(-i*.08);
  	fragColor=vec4(o*o+p.z,1);

}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}