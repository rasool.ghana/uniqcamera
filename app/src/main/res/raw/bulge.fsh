precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

// changes in 0.1 step
#define value 3.0

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 p = fragCoord.xy/iResolution.xy - 0.5;

  // cartesian to polar coordinates
  float r = length(p);
  float a = atan(p.y, p.x);

  // distort
  //r = sqrt(r)*value; // pinch
  r = r*r*value; // bulge

  // polar to cartesian coordinates
  p = r * vec2(cos(a)*0.5, sin(a)*0.5);

  // sample the iChannel0
  vec4 color = texture2D(iChannel0, p + 0.5);
  fragColor = color;
}



void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}