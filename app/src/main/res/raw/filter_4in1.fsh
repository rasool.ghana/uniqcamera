precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define PIXEL_SIZE 10.0

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 m_uv = vec2(uv.x >= 0.5 ? uv.x - 0.5 : uv.x, uv.y >= 0.5 ? uv.y - 0.5 : uv.y) * 2.0;
    vec4 col = texture2D(iChannel0, m_uv);
    if(uv.x <= 0.5 && uv.y <= 0.5){
        col = texture2D(iChannel0, m_uv);
    	float vcol = 10. * (mod(fragCoord.y / 5.,1.) - .45);
		float hCol = 10. * (mod(fragCoord.x / 5.,1.) - .85);
        float gray = hCol;
        if(col.r > 0.5) {
          gray = vcol;
        }
        col = vec4(vec3(gray),1.0);
    }else if(uv.x <= 0.5 && uv.y >= 0.5){
        col = texture2D(iChannel0, m_uv);
    	if(80. / 255. < col.r && col.r < 170. / 255. && 
           55. / 255. < col.g && col.g < 145. / 255. && 
           55. / 255. < col.b && col.b < 145. / 255.){
            col = vec4(0);
            float dx = 1.0 / iResolution.x;
            float dy = 1.0 / iResolution.y;
            m_uv.x = (dx * PIXEL_SIZE) * floor(m_uv.x / (dx * PIXEL_SIZE));
            m_uv.y = (dy * PIXEL_SIZE) * floor(m_uv.y / (dy * PIXEL_SIZE));

            for (int i = 0; i < int(PIXEL_SIZE); i++)
                for (int j = 0; j < int(PIXEL_SIZE); j++)
                    col += texture2D(iChannel0, vec2(m_uv.x + dx * float(i), m_uv.y + dy * float(j)));
            col /= pow(PIXEL_SIZE, 2.0);
        }
    }else if(uv.x >= 0.5 && uv.y <= 0.5){
        m_uv.x = 1.0 - m_uv.x;
        col = texture2D(iChannel0, m_uv);
    	col = vec4((col.r + col.g + col.b) / 3.);
    }else{
        m_uv.x = 1.0 - m_uv.x;
        col = texture2D(iChannel0, m_uv);
     	col = vec4(1.) - col;   
    }
    fragColor = col;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}