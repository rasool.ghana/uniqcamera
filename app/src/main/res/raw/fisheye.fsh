precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define blabla 178.0

const float PI = 3.1415926535;

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  float aperture = blabla;
  float apertureHalf = 0.5 * aperture * (PI / 180.0);
  float maxFactor = sin(apertureHalf);

    vec2 uvDef = fragCoord.xy / iResolution.xy;

  vec2 uv;
  vec2 xy = 2.0 * uvDef - 1.0;
  float d = length(xy);
  if (d < (2.0-maxFactor))
  {
    d = length(xy * maxFactor);
    float z = sqrt(1.0 - d * d);
    float r = atan(d, z) / PI;
    float phi = atan(xy.y, xy.x);

    uv.x = r * cos(phi) + 0.5;
    uv.y = r * sin(phi) + 0.5;
  }
  else
  {
    uv = uvDef;
  }
  vec4 c = texture2D(iChannel0, uv);
  fragColor = c;
}

void main() {
    mainImage(gl_FragColor,  texCoord*iResolution.xy);
}