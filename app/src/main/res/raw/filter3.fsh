precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 2.

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 c = texture2D(iChannel0,uv);
    float g = max(c.r,max(c.g,c.b))*value;
    // 1000 is changing value here
    float fuck = 345.678+1000.0;
    float f = mod((uv.x+uv.y+500.)*fuck,1.);
    if(mod(g,1.0)>f)
        c.r = ceil(g);
    else
        c.r = floor(g);
    c.r/=value;
	fragColor = c.rrra;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}