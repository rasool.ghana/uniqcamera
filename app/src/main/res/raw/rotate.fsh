precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 1.0

vec2 mirror(vec2 x)
{
	return abs(fract(x/2.0) - 0.5)*2.0;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = -1.0 + 2.0*fragCoord.xy / iResolution.xy;

	float a = iGlobalTime*0.2;

	for (float i = 1.0; i < 5.0; i += 1.0) {
		uv = vec2(sin(a)*uv.y - cos(a)*uv.x, sin(a)*uv.x + cos(a)*uv.y);
		uv = mirror(uv);

		// These two lines can be changed for slightly different effects
		// This is just something simple that looks nice
		a += value;
		//a /= i;

	}

	fragColor = texture2D(iChannel0, mirror(uv*2.0));
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}