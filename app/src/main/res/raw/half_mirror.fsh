precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;


#define value 400.0

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    float ping = (value+1.0)/iResolution.y;

    uv.x = 	mod(uv.x,ping*2.0);
    uv.x -=	ping;
    uv.x = 	abs(uv.x);
    uv.x += (1.0+1.0)/iResolution.x*(1.0-ping);

    vec4 c = texture2D(iChannel0,uv);

    uv.x *= 	-1.0;
    uv.x +=		ping;
    uv.x = 		abs(uv.x);

   	c += texture2D(iChannel0,uv);
	fragColor = c*0.5;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}