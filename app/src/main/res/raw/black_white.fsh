precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define PIXEL_SIZE 10.0

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    
    vec2 xy = fragCoord.xy / iResolution.xy;

    //xy.x = 1.0 - xy.x;
    
	vec4 textColor = texture2D(iChannel0, xy);
	
    float avg = (textColor.r + textColor.g + textColor.b) / 3.0;
    
   	fragColor = vec4( avg, avg, avg, textColor.a);
            
}


void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}