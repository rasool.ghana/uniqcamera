precision mediump float;

uniform vec3                iResolution;
uniform sampler2D           iChannel0;
uniform float               iGlobalTime;
varying vec2                texCoord;

#define value 10.0

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 c = texture2D(iChannel0,uv)*2.0;
    uv.xy+=c.bg*(value/iResolution.x-.5);
    uv-=.5;
    float a = atan(uv.y,uv.x);
    float d = length(uv);
    a+=c.r*(value/iResolution.y-.5)*12.0;
    uv.x = cos(a)*d;
    uv.y = sin(a)*d;
    uv+=.5;
    c = texture2D(iChannel0,uv)*2.0;
	fragColor = c;
}

void main() {
    mainImage(gl_FragColor,texCoord*iResolution.xy);
}